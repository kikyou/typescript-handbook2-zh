# TypeScript 手册（第二版）

## 前言

目前是 **DeepL** + 人工校对的方式，此文档并非纯翻译，译者在原文档的基础上：

- 删去了一些官方啰嗦的废话
- 修改了部分代码示例，让其看起来更简洁易懂
- 如有纰漏或错误，望各位读者联系我指正，谢谢！

希望给屏幕前的你带来良好的阅读体验 🤣

## 基础知识

首先来看我们在一个名为 `user` 的变量上进行的一些操作：

```ts
// 访问 user 上的 toLowerCase 属性，然后调用 user 本身
user.toLowerCase();
user();
```

可以发现，第一行访问了 `user` 上名为 `toLowerCase` 的属性，然后调用了这个属性；

第二行则试图直接调用 `user` 。

但是假设我们事先并不知道 `user` 的值--这是很常见的。那么问题来了 🧐

- `user` 是否可调用？
- 它是否有一个名为 `toLowerCase` 的属性？
- 如果有， `toLowerCase` 是否可以调用？
- 如果这两个值都是可调用的，它们的返回值是什么？

如果在代码运行前，能先检查上述问题，我们日常编写代码时就可以避免很多 **低级错误**

假设 `user` 是以下方式定义的：

```ts
const user = 'XiaoMing'; // ✨本文主角'小明'闪亮登场✨
```

显而易见，如果我们尝试运行第一行代码，将会正确的得到全小写的 `'xiao ming'` ，与我们的预期相符。

那第二行代码呢？如果你熟悉 JavaScript ，你就会知道这段代码运行会报错：

```ts
// TypeError: user is not a function
```

如果我们能避免犯这样的低级错误，那就太好了 😇

对于一些值，比如原始类型中的 `string` 和 `number` ，我们可以在运行时使用 `typeof` 操作符来识别其类型。但是对于其他的类型比如函数，就没有相应的运行时机制来识别它们的类型了。例如下面这个函数：

```ts
return user.getName(); // 获取 user 的名字
```

阅读代码可以发现， `user` 只有在拥有可调用的 `getName` 方法时才能正确运行。但是 JavaScript 并不能在代码运行前来显示这一信息。在纯的 JavaScript 中，验证上述代码会如何执行的唯一方法就是调用它。这使得我们很难在代码运行前预测其行为。

## 静态类型检查

为了解决上述问题，我们希望有一个工具来帮助我们在代码运行前发现这些错误。这就是 TypeScript 这样的静态类型检查器的作用。静态类型系统描述了当我们运行程序时，我们的值将会是什么样的，例如：

```ts
const user = 'xiaoming';
user();
// 此表达式不可调用
// 类型 'String' 没有调用签名
```

可以发现，用 TypeScript 运行上述示例代码时，将会有贴心的错误信息提示 😎

## 捕获异常

[ECMAScript 规范](https://tc39.github.io/ecma262/) 规定了遇到意外情况时必须有明确的指示。例如，试图调用不可调用的值应该抛出一个错误。这听起来是理所当然的，但是访问一个对象上不存在的属性是不是也应该抛出一个错误？很遗憾， JavaScript 返回的却是 `undefined`

为此，静态类型系统还必须决定哪些代码应该被标记为系统中的错误，即使它是不会立即抛出错误的**合法的** JavaScript 。在 TypeScript 中，下面的代码会产生一个 `sex` 未定义的错误：

```ts
const user = {
  name: 'xiaoming',
  age: 22,
};

user.sex;
// 类型 '{ name: string; age: number; }' 上不存在属性 'sex'.
```

TypeScript 还可以捕捉到很多其他类型的错误

例如，拼写错误：

```ts
const str = '小明最帅';

// 考考你的眼力😏...
str.toLocaleLowercase();
str.toLocalLowerCase();

// 其实我们有可能想表达的是这个😥...
str.toLocaleLowerCase();
```

函数没有被正确调用：

```ts
function fn() {
  // 本意是调用 user.getAge()😓
  return user.getAge < 18;
  // 运算符 '<' 不能应用于类型 '() => number' 和 'number'
}
```

或者基础的逻辑错误：

```ts
const name = user.name === 'xiaoming' ? '小明' : '路人';
if (value !== '小明') {
  // ...
} else if (value === '路人') {
  // 很遗憾，这个条件语句里的内容永远不会执行...
}
```

## 类型工具化

当在代码中出现错误时， TypeScript 可以捕获这些错误。这想必是极好的，但 TypeScript 也可以防止我们在一开始时就犯这些错误。

类型检查器可以检查诸如我们是否在变量和其他属性上访问了正确的属性。不仅如此，它也可以提示你可能要使用哪些属性。例如在编辑器中提供错误信息和代码补全：

```ts
const user = {
  name: 'xiaoming',
  age: 17,
};
user.na; // 这里IDE会有代码补全提示
```

一个支持 TypeScript 的编辑器可以提供**快速修复**来自动修复错误，或者**重构**来轻松地重新组织代码。使用导航功能可以跳转到一个变量的定义，或找到一个变量的所有引用。所有这些都是建立在类型检查器之上的，并且是完全跨平台的，所以你最喜欢的[IDE]("https://github.com/Microsoft/TypeScript/wiki/TypeScript-Editor-Support")很可能有 TypeScript 支持。

## `tsc` : TypeScript 编译器

我们一直在谈论类型检查，但我们还没有使用我们的类型检查器。让我们熟悉一下我们的新朋友 `tsc` -- TypeScript 编译器。首先，我们需要通过 `pnpm` 来获取它：

```ts
pnpm i -g typescript
// npm || yarn 早就不推荐了，推荐pnpm
```

> 这将在全局范围内安装 TypeScript 编译器 `tsc` 。如果你想从本地的 `node_modules` 包中运行 `tsc` ，你可以使用 `pnpx` 或类似的工具。

然后我们可以新建一个文件夹，并尝试编写我们的第一个 TypeScript 程序： `hello.ts`

```ts
console.log('Hello world!');
```

是不是有点惊讶，这个 Hello World 程序看起来与你在 JavaScript 中写的 Hello World 程序完全一样。现在让我们通过运行 tsc 命令对它进行类型检查，这个命令是由 TypeScript 包为我们安装的：

```bash
tsc hello.ts
```

**Tada!** 🤣

等等，**tada**到底是什么？我们运行了 `tsc` ，但什么也没发生！这就是所谓的**tada**。好吧，有点冷 😂，不过幸好没有类型错误，所以我们在控制台中没有得到任何输出，因为确实没有什么可输出的 😢

但是再检查一下 🧐--我们得到了一些文件输出。如果我们看一下我们的当前目录，我们会看到 `hello.ts` 旁边有一个 `hello.js` 文件。那是我们的 `hello.ts` 文件在 `tsc` 编译为普通 JavaScript 文件后的产物。其内容如下：

```ts
console.log('Hello world!');
```

在这种情况下， TypeScript 确实没有什么可以转换的，所以它看起来和我们平常写的 JavaScript 完全一样。

但是假如我们真的引入了一个类型检查错误呢？让我们重写 `hello.ts` :

```ts
function fn(name, age) {
  console.log(`Hello ${name}, 你的年龄是 ${age}!`);
}

fn('xiaoming');
```

如果我们再次运行 `tsc hello.ts` ，注意我们在命令行上得到了一个错误：

```ts
// 应有 2 个参数，但获得 1 个
// 未提供 "age" 的自变量
```

TypeScript 告诉我们，我们忘了给 `fn` 函数传递第二个参数。到目前为止，我们只写了标准的 JavaScript ，但类型检查仍然能够发现我们代码中的问题。幸好有你啊 🙏 TypeScript ！

## 指定类型

到现在为止，我们还没有告诉 TypeScript 什么是 `name` 或 `age` 。让我们修改下代码，告诉 TypeScript `name` 是一个字符串，而 `age` 应该是一个数字：

```ts
function fn(name: string, age: number) {
  console.log(`Hello ${name}, 你的年龄是 ${age}!`);
}
```

你可以把这个签名理解为 `fn` 需要一个 `string` 类型的 `name` ，和一个 `number` 类型的 `age` 。

声明类型后， TypeScript 可以告诉我们其他可能被错误调用的情况。比如说...

```ts
fn('xiaoming', '17');
// 类型 'string' 的参数不能赋给类型 'number' 的参数
```

相信聪明的你可以迅速修复这个错误 😏

```ts
fn('xiaoming', 17);
```

请记住，我们并不总是要写明确的类型注释。在大多数情况下，即使我们省略它们， TypeScript 也可以为我们推断出正确的类型。

```ts
let name = 'xiaoming';
```

尽管我们没有告诉 TypeScript `name` 的类型是 `string` ，但 TypeScript 还是能正确推断出来。所以，当类型系统能轻而易举的推断出类型时，最好不要多此一举，来为变量添加类型声明。

> Tips：如果你将鼠标悬停在这个词上，你的编辑器就会显示上面的代码示例的悬浮提示了

## 抹去类型

让我们看看当我们用 `tsc` 编译 `fn` 之后的代码：

```ts
'use strict';
function fn(name, age) {
  console.log('Hello ' + name + ', 你的年龄是 ' + age + '!');
}
fn('xiaoming', 17);
```

这里需要注意两点：

- 我们的 `name` 和 `age` 参数不再有类型注释。
- 我们的 '模板字符串'（那个使用反斜线（`字符）的字符串）被转换为带有连接符（+）的普通字符串。

首先解释第一点，类型注解不是 JavaScript 的一部分（或者说 ECMAScript），所以没有任何浏览器或其他运行时可以不加修改地运行 TypeScript 。这就是为什么 TypeScript 首先需要一个编译器--它需要某种方式来转换 TypeScript 代码，以便你可以运行它。大多数 TypeScript 代码会被抹去，类似的，这里我们的类型注释也将被完全抹去

> 请记住：类型注释永远不会改变你程序的运行时行为。

## 降级

与上述示例的另一个区别是，我们的模板字符串是由以下内容改写的

```ts
`Hello ${name}, 你的年龄是 ${age}!`;
```

将会编译为：

```ts
'Hello ' + name + ', 你的年龄是 ' + age + '!';
```

为什么会这样？

模板字符串是 ECMAScript 的一个版本的功能，称为 ECMAScript 2015。 TypeScript 有能力从较新的 ECMAScript 版本重构代码到较旧的版本，如 ECMAScript 3 或 ECMAScript 5（又称 ES3 和 ES5）。这种从较新或 '较高 '的 ECMAScript 版本向较旧或 '较低 '的版本转移的过程，称为 **降级** 。

默认情况下， TypeScript 将编译为 ES3 ，这是 ECMAScript 的一个非常老的版本。我们可以通过使用 `--target` 来指定一个更新的版本。使用 `--target es2015` 运行时， TypeScript 会将目标改为 ECMAScript 2015，这意味着代码应该能够在 ECMAScript 2015 支持的地方运行。所以运行 `tsc --target es2015 hello.ts` 将转换出以下内容：

```ts
function fn(name, age) {
  console.log(`Hello ${name}, 你的年龄是 ${age}!`);
}

fn('xiaoming', 17);
```

> 虽然默认的目标是 ES3，但目前绝大多数的浏览器都支持 ES2015。因此，大多数开发者可以安全地指定 ES2015 或以上作为目标，除非要兼容某些古董浏览器（IE：喷嚏 😷）。

## 更严格

TypeScript 的默认类型是可选的，推理采用最宽松的类型，并且没有检查潜在的 `null` / `undefined` 。如果你正在迁移现有的 JavaScript ，这可能是理想的第一步。

TypeScript 也提供了严格性设置。这些严格性设置将静态类型检查从一个开关（要么代码被检查，要么不被检查）变成了更接近于一个表盘的东西。你把这个转盘转得幅度越大， TypeScript 就会为你检查越多。这可能需要一些额外的配置，但是从长远来看，它是值得的，并将启用更准确、彻底的检查。一个新的代码库应该尽可能的打开严格性检查。

TypeScript 有几个类型检查的严格性标志，可以打开或关闭，除非另有说明，否则我们所有的例子都是在启用所有这些标志的情况下编写的。CLI 中的 `--strict` 标志，或者 `tsconfig.json` 中的 `'strict': true` ，可以同时开启它们，我们也可以单独配置 `noImplicitAny` 和 `strictNullChecks` 。

### `noImplicitAny`

回顾一下，在某些地方， TypeScript 并不试图为我们推断出任何类型，而是退回到最宽松的类型： `any` 。这不是可能发生的最糟糕的事情--毕竟，无论如何，回退到 `any` 只是普通的 JavaScript 体验。（AnyScript 名不虚传 🤣）

然而，使用 `any` 往往会违背使用 TypeScript 的初衷。你的程序越是类型化，你就会得到更多的验证，这意味着你在编码时犯的错误会更少。打开 `noImplicitAny` 标志将对任何类隐式具有 `any` 类型的变量标记错误。

### `strictNullChecks`

默认情况下，像 `null` 和 `undefined` 这样的值是可以赋值给任何其他类型的。但是忘记处理 `null` 和 `undefined` 是世界上无数错误的大头原因。 `strictNullChecks` 标志使处理 `null` 和 `undefined` 更加明确，使我们不必担心是否忘记处理 `null` 和 `undefined` 。

# 常用类型

在本章中，我们将介绍一些在 JavaScript 代码中最常见的数值类型，并解释在 TypeScript 中描述这些类型的相应方法。这并不是一个详尽的列表，未来的章节将描述更多命名和使用其他类型的方法。

类型也可以出现在许多地方，而不仅仅是类型注释。随着我们对类型本身的了解，我们还将了解我们可以引用这些类型来形成新类型的地方。

我们将首先回顾你在编写 JavaScript 或 TypeScript 代码时可能遇到的最基本和最常见的类型。这些类型以后将形成更复杂的类型的核心构件。

## 原始类型： `string number boolean`

JavaScript 有三个非常常用的[原始类型](https://developer.mozilla.org/en-US/docs/Glossary/Primitive): `string` 、 `number` 和 `boolean` 。如果你在这些类型的值上使用 `typeof` 操作符，你会发现 `typeof` 值的结果与其对应的原始类型一致：

```ts
const str: string = 'str';
console.log(typeof str); // 'string'
```

- `string` 表示字符串值，如 ` 'Hello, world'`
- `number` 表示数字，如 `17` 。 JavaScript 对整数没有特殊的运行时值，所以没有等同于 `int` 或 `float` 的值--一切都是 `number` 🤗
- `boolean` 是指两个值 `true` 和 `false`

> 类型名称 `String`、 `Number` 和 `Boolean` （以大写字母开头）是合法的，但指的是一些特殊的内置类型。你应始终使用 `string` 、 `number` 或 `boolean` 来表示类型。

## 数组

要指定像 `[1, 2, 3]` 这样的数组的类型，你可以使用 `number[]` 这种语法；这种语法适用于任何类型（例如 `string[]` 是字符串的数组，以此类推）。你也可能看到它被写成 `Array<number>` ，这是等价的写法，但是在 `JSX` 中，只有前者的语法是允许的。

> 注意， `[number]` 与 ` number[]` 并不相同，具体差异请参考元组类型一节。

### `any`

TypeScript 也有一个特殊的类型， `any` ，当你不希望一个特定的值导致类型检查错误时，你可以使用它。

当一个值是 `any` 类型时，你可以访问它的任何属性（属性也是 `any` 类型），像函数一样调用它，把它赋给任何类型的值（或把任何类型的值赋给它），或几乎任何其他语法上合法的东西。

```ts
let user: any = { name: 'xiaoming' };
// 下面所有的代码都不会有编译器错误
// 使用 any 将不会触发任何类型检查
user.getAge();
user();
user.name = 100;
user = 'xiaohong';
const num: number = user;
```

当你不想写出一个长的类型，但是又想让 TypeScript 相信某一行代码是正常的时候， `any` 类型就很有用。

#### `noImplicitAny`

当你没有指定一个类型，并且 TypeScript 不能从上下文中推断出它，编译器通常会默认为 `any` 。

不过，我们应该避免这样做，因为 `any` 并没有类型检查。使用编译器标志 `noImplicitAny: "true"` 来标记任何隐式 `any` 类型为错误。

## 类型声明

当你使用 `const` 、 `var` 或 `let` 声明一个变量时，你可以选择添加一个类型注解来明确指定变量的类型。

```ts
let name: string = 'xiaoming';
```

> TypeScript 不使用 '类型在左边 '式的声明，如 `int x = 0` 。类型注释总是在变量的右边

不过在大多数情况下，你并不需要显式地声明类型。 TypeScript 会尝试在你的代码中自动推断出类型。例如，变量的类型是根据其初始值的类型来推断的：

```ts
// 不需要类型声明 -- 'name' 将被推断为 'string'
let name = 'xiaoming';
```

在大多数情况下，你并不需要深入理解类型推断的规则。如果你是刚入坑的新人，请尝试使用比你预期的更少的类型注释--你可能会惊讶地发现， TypeScript 只需要很少的类型注释，就能完全理解代码的含义。

## 函数

函数是 JavaScript 中传递数据的主要手段。 TypeScript 允许你指定函数的输入和输出值的类型。

### 参数类型声明

当你声明一个函数时，你可以在每个参数后面添加类型注释，来声明该函数接受什么类型的参数。参数类型注解一般声明在参数名称的后面。

```ts
// 这里声明 name 需为 string 类型
function greet(name: string) {
  console.log('Hello, ' + name.toUpperCase() + '!!');
}
```

当一个参数有类型注释时，该函数的参数将被检查是否符合类型注释：

```ts
greet(24);
// 类型 'number' 的参数不能赋给类型 'string' 的参数
```

> 即使你的参数没有类型注释， TypeScript 仍然会检查你是否传递了正确的参数数量。

### 返回值类型声明

你也可以添加返回类型的注释。返回类型注释添加在参数列表的后面：

```ts
function getAge(): number {
  return 17;
}
```

与变量类型注解一样，你通常并不需要返回类型注解，因为 TypeScript 会根据函数的返回语句来自动推断它的返回类型。

### 匿名函数

匿名函数与函数声明有一点不同。当一个函数出现在 TypeScript 可以确定它如何被调用的地方时，该函数的参数会自动被赋予类型。

这里有一个例子：

```ts
// 尽管没有声明类型, 但是 TypeScript 仍然能发现错误
const names = ['Alice', 'Bob', 'Eve'];

// 函数上下文类型
names.forEach(i => {
  console.log(i.toUppercase());
  // 类型 'string' 上不存在属性 'toUppercase'. 你是说 'toUpperCase' 嘛?🥰
});
```

尽管参数 `i` 没有类型注释， TypeScript 使用 `forEach` 函数的类型，以及数组的推断类型，来确定 `i` 的类型。

## 对象类型

除了原始类型之外，你最常遇到的类型是 `object` 类型。这指的是任何带有属性的 JavaScript 值。为了定义一个对象类型，我们简单地列出它的属性和它们的类型。

例如，下面这个函数接收一个表示 `user` （包含 `name` 和 `age` ）的对象：

```ts
// 参数类型声明为 object 类型
function logUser(user: { name: string; age: number }) {
  console.log('名字是 ' + user.name);
  console.log('年龄是 ' + user.age);
}
logUser({ name: 'xiaoming', age: 17 });
```

在这里，我们给参数注解了一个有两个属性的类型-- `name` 和 `age` 。你可以用 `,` 或 `;` 来分隔这些属性，并且最后一个分隔符是可选的。

每个属性的类型部分也是可选的。如果你没有指定一个类型，它将被假定为 `any` 。

### 可选属性

对象类型可以指定其部分或全部属性是可选的，只需在属性名称后面添加一个 `?` 修饰符即可：

```ts
function logUser(user: { name: string; age?: number }) {
  // ...
}
// 以下代码都将正常运行
logUser({ name: 'xiaoming' });
logUser({ name: 'xiaoming', age: 17 });
```

## 联合类型

TypeScript 的类型系统允许你使用大量的操作符从现有的类型中建立新的类型。现在我们知道如何编写一些类型，是时候开始以有趣的方式组合它们了 😏

### 定义联合类型

联合类型是一个由两个以上其他类型组成的类型，代表可能是这些类型中任何一个的值。我们把这些类型中的每一个称为联合类型的成员，并将这些类型用 `|` 隔开。

让我们写一个可以操作 `string` 或者 `number` 的函数：

```ts
function logId(id: number | string) {
  console.log('你的 id 是： ' + id);
}
// OK
logId(101);

// OK
logId('202');

// Error
// 类型 '{ myID: number; }' 的参数不能赋给类型 'string | number' 的参数
logId({ myID: 22342 });
```

## 使用联合类型

提供一个与联合类型相匹配的值是很容易的--只需提供与联合类型的任意一个成员相匹配的类型即可。

TypeScript 只允许你访问联合类型的每个成员共有的属性和方法。例如，如果你有一个联合类型 `string | number` ，你就不能使用 `string` 独有的方法：

```ts
function printId(id: number | string) {
  console.log(id.toUpperCase());
  // 属性 'toUpperCase' 不存在于类型 'string | number' 上.
  // 属性 'toUpperCase' 不存在于类型 'number'上.
}
```

我们可以通过收窄联合类型解决上述问题。当 TypeScript 可以为一个值推断出一个更具体的类型时，就会自动收窄类型。

例如， TypeScript 知道只有 `string` 类型的值才会使 `typeof === 'string'` 成立：

```ts
function printId(id: number | string) {
  if (typeof id === 'string') {
    // 在这个分支，id 的类型为 'string'
  } else {
    // id 在这里的类型为 'number'
  }
}
```

另一个例子是使用类似 `Array.isArray` 这样的函数：

```ts
function welcomePeople(x: string[] | string) {
  if (Array.isArray(x)) {
    // 这里： 'x' 的类型是 'string[]'
  } else {
    // 这里： 'x' 的类型是 'string'
  }
}
```

注意，在 `else` 分支中，我们并不需要做其他事--如果 `x` 不是一个 `string[]` ，那么它一定是一个 `string` 。

有时你会有一个联合类型，其中所有的成员都有一些共同属性和方法。例如，数组和字符串都有一个 `slice` 方法。如果一个联合类型中的每个成员都有一个共同的属性，你就可以直接使用这个属性而不需要收窄类型：

```ts
// 返回类型将被推断为 number[] | string
function getFirstThree(x: number[] | string) {
  return x.slice(0, 3);
}
```

## 类型别名

当我们想复用同一个类型时，我们可以用一个名字来表示这个类型。一个类型别名正是如此--任何类型的名字。类型别名的语法是：

```ts
type Point = {
  x: number;
  y: number;
};

function logPoint(pt: Point) {
  console.log('横坐标是 ' + pt.x);
  console.log('纵坐标是 ' + pt.y);
}

logPoint({ x: 100, y: 100 });
```

实际上，你可以使用一个类型别名来给任何类型命名。例如，一个类型别名可以命名一个联合类型：

```ts
type ID = number | string;
```

## 接口

> 接口是声明对象的另一种方法

```ts
interface Point {
  x: number;
  y: number;
}

function logPoint(pt: Point) {
  console.log('横坐标是 ' + pt.x);
  console.log('纵坐标是 ' + pt.y);
}

logPoint({ x: 100, y: 100 });
```

TypeScript 只关心我们传递给 `logPoint` 的值的结构--它只关心它是否有预期的属性。只关注类型的结构和能力，这就是为什么我们称 TypeScript 为结构化类型系统。

### 类型别名和接口之间的区别

类型别名和接口非常相似，在大多数情况下，你可以在它们之间自由选择。几乎所有 `interface` 的功能都可以在 `type` 中使用，关键的区别是，类型不能被重新扩展以添加新的属性，而接口总是可扩展的。

**Interface**

```ts
interface Animal {
  name: string;
}

interface Bear extends Animal {
  honey: boolean;
}

// Bear等价为：
// interface Bear {
//   name: string;
//   honey: boolean;
// }

interface Window {
  title: string;
}

interface Window {
  name: string;
}

// Window等价为：
// interface Window {
//   title: string;
//   name: string;
// }
```

**type**

```ts
type Animal = {
  name: string;
};

type Bear = Animal & {
  honey: Boolean;
};

// Bear等价为：
// type Bear {
//   name: string;
//   honey: boolean;
// }

type Window = {
  title: string;
};

type Window = {
  ts: string;
};
// 错误： 重复声明 'Window'.
```

你将在后面的章节中学习更多关于这些概念的内容，所以如果你暂时不能一次性理解这么多内容，也不必担心。

- 类型别名不能参与声明合并，但接口可以。
- 接口只能用于声明对象的形状，而不是重新命名原始类型。
- 接口名称将总是以其原始形式出现在错误信息中，但只有当它们被命名使用时。

在大多数情况下，你可以根据个人喜好来选择，但是我们你推荐尽可能的使用 `interface` ，直到你需要使用来自 `type` 的特性。

## 类型断言

有时你会有关于一个值的类型的信息，而 TypeScript 却不知道。

例如，如果你使用 `document.getElementById` ， TypeScript 只知道这将返回某种 `HTMLElement` ，但你可能知道上面的代码将更加具体的返回 `HTMLCanvasElement` 。

在这种情况下，你可以使用一个类型断言来指定一个更具体的类型：

```ts
const myCanvas = document.getElementById('main_canvas') as HTMLCanvasElement;
```

与类型注解一样，类型断言将被编译器删除，不会影响你的代码的运行时行为。

你也可以使用角括号 `<>` 语法（不能在 JSX 中使用），这两种写法是等价的：

```ts
const myCanvas = <HTMLCanvasElement>document.getElementById('main_canvas');
```

> 请注意：因为类型断言是在编译时删除的，所以在运行时不会有任何类型断言的检查。如果类型断言是错误的，就不会有异常或 `null` 产生。

TypeScript 只允许类型断言转换为一个类型的更具体或更不具体的版本。这条规则防止了 '不可能 '的转换，比如：

```ts
const x = 'hello' as number;
// 看见了没这就是不可能的转换
```

有时这条规则可能过于保守，并且会禁止更复杂的类型断言（即使断言可能是正确的）。如果发生这种情况，你可以使用两个断言，首先断言为 `any` （或 `unknown` ，我们将在后面介绍），然后再断言为你需要的类型：

```ts
const a = expr as any as T;
```

## 字面量类型

字面量类型主要分为三种：

- 字符串字面量
- 数字字面量
- 布尔字面量

这些字面量只能被赋值为其定义的联合类型之一：

```ts
// str 只能被赋值为 'left' | 'right' | 'center' 其中之一
type str = 'left' | 'right' | 'center';
type num = 0 | 1 | 2 | 3;
type bool = true | false;

const Str: str = 'top';
// 不能将类型 'top' 分配给类型 str
```

### 字面量推断

当你用一个对象初始化一个变量时， TypeScript 假设该对象的属性以后可能会改变值。例如，如果你写了这样的代码：

```ts
const obj = { count: 0 };
if (bool) {
  obj.count = 1;
}
```

TypeScript 不认为将 `1` 赋值给之前为 `0` 的 `count` 是一个错误。或者说， `obj.counter` 必须是 `number` 类型，而没有要求必须是 `0` 。

这同样适用于字符串：

```ts
const req = { url: 'https://example.com', method: 'GET' };
handleRequest(req.url, req.method);
// 不能将类型 'string' 分配给类型 ''GET' | 'POST''.
```

在上面的例子中， `req.method` 被推断为 `string` ，而不是 `'GET'` 。因为 `req.method` 可能会被赋值为别的字符串，如 `'GUESS'` ，所以 TypeScript 认为这段代码有一个错误。

有两种方法可以解决这个问题。

- 在任一位置添加一个类型断言来改变推论：

  ```ts
  const req = { url: 'https://example.com', method: 'GET' as 'GET' };
  handleRequest(req.url, req.method as 'GET');
  ```

  第一行表示 `req.method` 总是具有字面类型 `'GET'` ，防止之后可能对该字段分配 ` 'GUESS'` 。

  第二行表示直接断言 `req.method` 的值是 ` 'GET'` 。

- 使用 `as const` 来将整个对象转换为字面量类型：

  ```ts
  const req = { url: 'https://example.com', method: 'GET' } as const;
  handleRequest(req.url, req.method);
  ```

`as const` 后缀的作用类似于 `const` ，作用是使所有属性都被分配到字面类型，而不是像 `string` 或 `number` 这样的更泛型的类型。

## `null` 和 `undefined`

JavaScript 有两个原始值，用来表示空值或未初始化的值： `null` 和 `undefined` 。

TypeScript 也有两个相同名称的对应类型。这些类型的行为方式取决于你是否打开了 `strictNullChecks` 选项。

### `strictNullChecks` 关闭

在关闭 `strictNullChecks` 的情况下，可能为 `null` 或 `undefined` 的值仍然可以被正常访问，并且 `null` 和 `undefined` 的值可以被分配给任何类型的属性。这类似于没有空值检查的语言（例如 C#、Java）的表现。缺乏对这些值的检查往往是错误的主要来源；我们总是建议人们在他们的 `tsconfig.json` 打开 `strictNullChecks` 。

### `strictNullChecks` 开启

开启 `strictNullChecks` 后，当一个值为 `null` 或 `undefined` 时，你需要在对该值使用方法或属性前测试这些值。就像在访问一个可选属性之前需要检查 `undefined` 一样，我们可以使用类型收窄来检查可能为 `null` 的值：

```ts
function doSomething(x: string | null) {
  if (x === null) {
    // 什么事也不干
  } else {
    console.log('Hello, ' + x.toUpperCase());
  }
}
```

## 非空断言操作符 ！

TypeScript 也有一个特殊的语法，用于从一个类型中移除 `null` 和 `undefined` ，而不做任何显式检查。在任何表达式后面写上 `!` 实际上是一个类型断言，即断言该值不是 `null` 或 `undefined` :

```ts
function liveDangerously(x?: number | null) {
  // 不会报错
  console.log(x!.toFixed());
}
```

就像其他类型的断言一样，这不会改变你的代码的运行时，所以仅在你知道值不是 `null` 或 `undefined` 时才使用 `!`

# 类型收窄

假设我们有一个叫 `logName` 的函数：

```ts
function logName(name: number | string): string {
  // to do sth...
}
```

然后在函数体内编写以下代码：

```ts
function logName(name: number | string): string {
  return `欢迎${name.toLocaleUpperCase()}`;
  // 类型'string | number'上不存在属性'toLocaleUpperCase'。
  // 类型'number'上不存在属性'toLocaleUpperCase'。
}
```

Oops，出错了。 TypeScript 警告我们，在一个 `number | string` 类型的值上调用 `toLocaleUpperCase` 可能不会像我们预期的那样。我们并没有事先检查 `name` 是否为 `number` 类型，并处理 `number` 类型的情况，所以我们可以这么写：

```ts
function logName(name: number | string): string {
  if (typeof name === 'string') {
    return `欢迎${name.toLocaleUpperCase()}`;
  }
  return `欢迎第${name}位客人`;
}
```

TypeScript 的类型系统旨在使编写典型的 JavaScript 代码变得尽可能容易，而不需要费尽心思去获得类型安全。

在 TypeScript 中， 类型收窄有很多种不同的方式：

### `typeof` 类型守卫

JavaScript 支持一个 `typeof` 操作符，它可以提供关于我们在运行时的值的类型的基本信息。 `typeof` 有一组特定的返回值：

- `'string'`
- `'number'`
- `'bigint'`
- `'boolean'`
- `'symbol'`
- `'undefined'`
- `'object'`
- `'function'`

TypeScript 可以理解 `typeof` 其含义，并进一步收窄不同代码分支中的类型。在 TypeScript 中，针对 `typeof` 返回的值的检查是一个类型守卫。注意， `typeof` 并不会返回字符串 `'null'` 。请看下面的例子：

```ts
function log(strs: string | string[] | null) {
  if (typeof strs === 'object') {
    for (const s of strs) {
      // 注意，strs 有可能为 'null'.
      console.log(s);
    }
  } else if (typeof strs === 'string') {
    console.log(strs);
  } else {
    // 什么也不做
  }
}
```

在 `log` 函数中，我们试图检查 `strs` 是否是一个对象，看它是否为 `string[]` 类型（现在可能是一个很好的时机来强化 **数组** 在 JavaScript 中是 **对象** 类型这个知识点）。但是在 JavaScript 中 `typeof null === 'object'` 很遗憾 😢，历史遗留问题。

老鸟们可能微微一笑，但并不是每个人都知道这个坑。幸运的是， TypeScript 可以提醒我们， `strs` 只是收窄到 `string[] | null` ，而不是只有 `string[]` 。

这可能是一个很好的切入点，我们称之为 '真实性 '检查。

### 真实性收窄

`Truthiness` 这个单词可能你在字典里找不到，但在 JavaScript 中你应该会听到很多次。

在 JavaScript 中，我们可以在条件语句中使用任何表达式， `&&s` 、 `||s` 、 `if` 语句、布尔否定句（ `!` ），等等。总的来说，条件语句表达式并不总是布尔类型。

```ts
// 检查有多少用户在线
function checkOnline(users: number) {
  if (users) {
    return `目前有 ${users} 位用户在线!`;
  }
  return '一个人也没有 :(';
}
```

在 JavaScript 中，像 `if` 这样的语句会首先将其条件表达式 **强制** 转换为布尔类型，以使其具有意义，然后根据结果是 `true` 还是 `false` 来选择其分支。像以下值：

- `0`
- `NaN`
- `''`
- `0n`
- `null`
- `undefined`

都被强制转换为 `false` ，而其他值则被强制转换为 `true` 。你总是可以通过运行 `Boolean(xx)` ，或使用更短的 `!!` 操作符，将值直接强制转换为布尔值：

```ts
Boolean('hello'); // type: boolean, value: true
!!'world'; // type: true,    value: true
```

这种方法对于排除 `null` 或 `undefined` 等值很有效。例如：

```ts
function log(strs: string | string[] | null) {
  if (!!strs && typeof strs === 'object') {
    for (const s of strs) {
      console.log(s);
    }
  } else if (typeof strs === 'string') {
    console.log(strs);
  }
}
```

你会注意到，我们通过检查 `strs` 是否为 `Truthy` 而摆脱了上面的错误。这至少可以防止我们在运行代码时出现可怕的错误，比如：

```ts
// 类型错误： null 不可迭代
```

但请记住，对原始类型的真实性检查往往是容易出错的。作为一个例子，考虑一下编写 `log` 的不同尝试

```ts
function log(strs: string | string[] | null) {
  // !!!!!!!!!!!!!!!!
  //  不能这么写!
  // !!!!!!!!!!!!!!!!
  if (strs) {
    if (typeof strs === 'object') {
      for (const s of strs) {
        console.log(s);
      }
    } else if (typeof strs === 'string') {
      console.log(strs);
    }
  }
}
```

我们将整个函数的主体包裹在一个真值检查中，但这里一个不易察觉的错误：我们可能不再正确处理空字符串( `'' ` )的情况。

最后提一嘴，可以使用 `!` 从被否定的分支中过滤掉不符合条件的类型：

```ts
function multiplyAll(values: number[] | undefined): number[] | undefined {
  if (!values) {
    return values;
  } else {
    return values.map(x => x);
  }
}
```

### 等号收窄

TypeScript 还能使用 `switch` 语句和等号检查，如 `===` ， `!==` ， `==` ，和 `!=` 来收窄类型。比如：

```ts
function example(x: string | number, y: string | boolean) {
  if (x === y) {
    // 现在我们可以 'x' 或者 'y' 上调用任何字符串方法.
    x.toUpperCase();
    // (method) String.toUpperCase(): string
    y.toLowerCase();
    // (method) String.toLowerCase(): string
  } else {
    console.log(x);
    // (parameter) x: string | number
    console.log(y);
    // (parameter) y: string | boolean
  }
}
```

当我们在上例中检查 `x` 和 `y` 都是相等的时候， TypeScript 知道它们的类型一定也是相同的。由于 `string` 是 `x` 和 `y` 的唯一共有类型，所以 TypeScript 知道 `x` 和 `y` 在第一个分支中一定是 `string` 类型。

针对特定字面值（相对于变量）的检查也是如此。在我们关于真实性收窄的章节中，我们写了一个 `log` 函数，因为它不小心没有正确处理空字符串很容易出问题。所以，我们可以做一个特定的检查来阻止空字符串，而且 TypeScript 仍然可以正确地从字符串的类型中删除空字符串。

```ts
function log(strs: string | string[] | null) {
  if (strs !== null) {
    if (typeof strs === 'object') {
      for (const s of strs) {
        // (parameter) strs: string[]
        console.log(s);
      }
    } else if (typeof strs === 'string') {
      console.log(strs);
      // (parameter) strs: string
    }
  }
}
```

JavaScript 比较宽松的等号检查 `==` 和 `!=` 也被正确地收窄了。如果你不熟悉，检查一个东西是否 `==null` 实际上不仅检查它是否具体是 `null` 这个值--它还检查它是否有可能是 `undefined` 。这同样适用于 `==undefined` :它检查一个值是否是 `null` 或 `undefined` 。

```ts
interface Container {
  value: number | null | undefined;
}

function multiplyValue(container: Container, factor: number) {
  // 从类型中同时移除 'null' and 'undefined' .
  if (container.value != null) {
    console.log(container.value);
    // (property) Container.value: number
    // 现在我们可以安全的计算 'container.value'.
    container.value *= factor;
  }
}
```

### `in` 操作符

JavaScript 有一个操作符来确定一个对象包含一个特定的属性： `in` 操作符。 TypeScript 考虑到了这一点，将其作为一种收窄潜在类型的方式。

例如，下面这段代码：

```ts
type Fish = { swim: () => void };
type Bird = { fly: () => void };

function move(animal: Fish | Bird) {
  if ('swim' in animal) {
    // 可以确定在这个分支里 animal 为 Fish类型
    return animal.swim();
  }
  // 这里 animal 一定为 Bird类型
  return animal.fly();
}
```

假设一个人可以同时游泳和飞行（假设它是超人），那么它将同时出现在 `if` 和 `else` 两个分支中：

```ts
type Fish = { swim: () => void };
type Bird = { fly: () => void };
type Human = { swim?: () => void; fly?: () => void };

function move(animal: Fish | Bird | Human) {
  if ('swim' in animal) {
    console.log(animal);
    // (parameter) animal: Fish | Human
  } else {
    console.log(animal);
    // (parameter) animal: Bird | Human
  }
}
```

### `instanceof` 操作符

JavaScript 有一个操作符来检查一个值是否是另一个值的 **实例**。更具体地说，在 JavaScript 中 `x instanceof Foo` 检查 `x` 的原型链上是否包含 `Foo.prototype` 。正如你可能已经猜到的， `instanceof` 也是一个类型守卫， TypeScript 收窄了由 `instanceof` 守卫的分支：

```ts
function logValue(x: Date | string) {
  if (x instanceof Date) {
    console.log(x.toUTCString());
    // (parameter) x: Date
  } else {
    console.log(x.toUpperCase());
    // (parameter) x: string
  }
}
```

## 赋值

正如我们前面提到的，当我们对任何变量进行赋值时， TypeScript 会查看赋值的右侧，并适当收窄左侧的类型范围。

```ts
let x = Math.random() < 0.5 ? 10 : 'hello world!';
// let x: string | number

x = 1;
console.log(x);
// let x: number

x = 'goodbye!';
console.log(x);
// let x: string
```

请注意，这些赋值中的每一个都是有效的。这是因为 x 的声明类型是 `string | number` ，而可赋值性总是根据声明类型来检查。

如果我们给 `x` 分配一个布尔值，我们会看到一个错误，因为那并不是声明类型的一部分：

```ts
let x = Math.random() < 0.5 ? 10 : 'hello world!';
// let x: string | number

x = 1;
console.log(x);
// let x: number

x = true;
// 不能将类型 'boolean' 分配给类型 'string | number'.

console.log(x);
// let x: string | number
```

## 使用类型断言

到目前为止，我们已经可以使用现有的 JavaScript 结构来收窄类型，然而有时你想更直接地控制整个代码中的类型变化。

为了定义一个用户定义的类型守卫，我们只需要定义一个函数，其返回类型是一个类型断言：

```ts
function isFish(pet: Fish | Bird): pet is Fish {
  return (pet as Fish).swim !== undefined;
}
```

在这个例子中， `pet is Fish` 是我们的类型断言。谓词的形式是 `parameterName is Type` ，其中 `parameterName` 必须是当前函数签名中的参数名称之一。

任何时候 `isFish` 被调用时，如果类型断言兼容函数的返回值类型， TypeScript 将把该变量收窄到我们断言的特定类型：

```ts
// 现在调用 'swim' and 'fly' 都是 ok 的.
let pet = getSmallPet();

if (isFish(pet)) {
  pet.swim();
  // fish
} else {
  pet.fly();
  // bird
}
```

注意， TypeScript 不仅知道 `pet` 在 `if` 分支中是一条 `fish` ；它还知道在 `else` 分支中， `pet` 一定是一只 `bird` 。

你可以使用类型守卫 `isFish` 来过滤 `Fish | Bird` 的数组，获得纯 `Fish` 的数组。

```ts
const zoo: (Fish | Bird)[] = [getSmallPet(), getSmallPet(), getSmallPet()];
const underWater1: Fish[] = zoo.filter(isFish);
```

此外，类可以使用 `this is Type` 来收窄其类型。

## 可辨识联合

到目前为止，我们所看的大多数例子都是围绕着用简单的类型（如字符串、布尔值和数字）来收窄单个变量。虽然这很常见，但在 JavaScript 中，大多数时候我们要处理的是稍微复杂的结构。

为了激发灵感，让我们想象一下，我们正试图对圆形和方形等形状进行编码。圆形将有一个 `radius` 属性用于记录半径，方形将有一个属性 `sideLength` 用于记录边长。我们将使用一个叫做 `kind` 的字段来告诉我们正在处理的是哪种形状。以下是我们定义 `Shape` 的第一次尝试：

```ts
interface Shape {
  kind: 'circle' | 'square';
  radius?: number;
  sideLength?: number;
}
```

`'circle '` 和 `'square '` 分别告诉我们应该把这个形状当作一个圆形还是方形。通过使用 ` 'circle' | 'square '` 而不是 `string` ，我们还可以避免拼写错误的问题。然后我们可以编写一个 `getArea` 函数，根据它处理的是圆形还是方形来应用正确的逻辑。我们首先尝试处理圆形：

```ts
function getArea(shape: Shape) {
  return Math.PI * shape.radius ** 2;
  // radius 可能为 'undefined'.
}
```

在 `strictNullChecks` 下，这给了我们一个错误--这是很恰当的，因为 `radius` 可能没有被定义。但是假如我们对 `kind` 属性进行适当的检查呢？

```ts
function getArea(shape: Shape) {
  if (shape.kind === 'circle') {
    return Math.PI * shape.radius ** 2;
    // radius 可能为 'undefined'.
  }
}
```

emm... TypeScript 仍然不知道该怎么做。为了解决问题，我们可以尝试使用非空断言符（ `shape.radius` 加一个 `!` ）来假定 `radius` 肯定存在：

```ts
function getArea(shape: Shape) {
  if (shape.kind === 'circle') {
    return Math.PI * shape.radius! ** 2;
  }
}
```

但上述代码并不理想。我们不得不强制使用非空断言，以说服 TypeScript 相信 `shape.radius` 是存在的，一旦我们在代码其他地方做了修改，非空断言就很容易出错。此外，在 `strictNullChecks` 之外，我们也可能意外地访问到这些字段（因为在读取这些字段时，可选属性被认为总是存在的）。其实我们绝对可以做得更好。

Shape 的这种语法的问题是，类型检查器没有办法根据种类属性知道 `radius` 或 `sideLength` 是否存在。我们需要把我们知道的东西传达给类型检查器。考虑到这一点，让我们重新定义一下 `Shape` :

```ts
interface Circle {
  kind: 'circle';
  radius: number;
}

interface Square {
  kind: 'square';
  sideLength: number;
}

type Shape = Circle | Square;
```

在这里，我们正确地将 `Shape` 分成了两种类型，为 `kind` 属性设置了不同的值，但是 `radius` 和 `sideLength` 在它们各自的类型中被声明为必需的属性。

让我们看看当我们试图访问 `Shape` 的 `radius` 时会发生什么：

```ts
function getArea(shape: Shape) {
  return Math.PI * shape.radius ** 2;
  // 属性 'radius' 并不存在于类型 'Shape'.
  // 属性 'radius' 并不存在于类型 'Square'.
}
```

就像我们对 Shape 的第一个定义一样，上述代码仍然有错误。当 `radius` 是可选的时候，我们得到了一个错误（仅在 `strictNullChecks` 中），因为 TypeScript 无法判断该属性是否存在。现在 `Shape` 是一个联合类型， TypeScript 告诉我们 `shape` 可能是一个 `Square` ，而 Square 并没有 `radius` 属性。

但是，如果我们再次尝试检查 `kind` 属性呢？

```ts
function getArea(shape: Shape) {
  if (shape.kind === 'circle') {
    return Math.PI * shape.radius ** 2;
    // (parameter) shape: Circle
  }
}
```

这就摆脱了错误！ 当联合类型中的每个类型都包含一个共同属性时， TypeScript 将识别这是一个可辨识联合，并且可以收窄联合类型的成员范围。

在这种情况下， `kind` 就是那个共同属性（这就是判别 `Shape` 的依据）。检查 `kind` 属性是否为 `Circle` ，就可以剔除 `Shape` 中所有没有 `Circle` 类型属性的类型。这就把 `Shape` 的范围收窄到了 `Circle` 这个类型。

同样的检查方法也适用于 `switch` 语句。现在我们可以试着编写完整的 `getArea` ，而不需要任何讨厌的非空断言( `!` )

```ts
function getArea(shape: Shape) {
  switch (shape.kind) {
    case 'circle':
      return Math.PI * shape.radius ** 2;
    // (parameter) shape: Circle
    case 'square':
      return shape.sideLength ** 2;
    // (parameter) shape: Square
  }
}
```

> 作为一个旁观者，试着玩一玩上面的例子，去掉一些返回关键词。你会发现，类型检查可以帮助避免在 switch 语句中不小心落入不同子句的错误。

## `never` 类型

在收窄类型时，你可以将一个联合类型的选项减少到--你已经删除了所有的可能性并且什么都不剩的程度。在这些情况下， TypeScript 将使用一个 never 类型来代表一个不应该存在的状态。

## 彻底的检查

`never` 类型可以分配给每个类型；但是，没有任何类型可以分配给 `never` （除了 `never` 本身）。这意味着你可以使用类型收窄并使用 `never` 在 `switch` 语句中做详尽的检查。

例如，在我们的 `getArea` 函数中添加一个默认值，试图将 `Shape` 分配给 `never` ，当每个可能的情况都没有被处理时，就会触发：

```ts
type Shape = Circle | Square;

function getArea(shape: Shape) {
  switch (shape.kind) {
    case 'circle':
      return Math.PI * shape.radius ** 2;
    case 'square':
      return shape.sideLength ** 2;
    default:
      const _exhaustiveCheck: never = shape;
      return _exhaustiveCheck;
  }
}
```

在 `Shape` 联合类型中添加一个新成员，将导致 TypeScript 错误：

```ts
interface Triangle {
  kind: 'triangle';
  sideLength: number;
}

type Shape = Circle | Square | Triangle;

function getArea(shape: Shape) {
  switch (shape.kind) {
    case 'circle':
      return Math.PI * shape.radius ** 2;
    case 'square':
      return shape.sideLength ** 2;
    default:
      const _exhaustiveCheck: never = shape;
      // Type 'Triangle' is not assignable to type 'never'.
      return _exhaustiveCheck;
  }
}
```

# 函数进阶

函数是任何应用程序的基本构件，无论它们是本地函数，从另一个模块导入，还是一个类上的函数。它们也是值，就像其他值一样， TypeScript 有很多方法来描述如何调用函数。让我们来学习一下如何编写描述函数的类型。

## 函数类型表达

描述一个函数的最简单方法是编写一个函数类型表达式，在语法上类似于箭头函数：

```ts
function greet(fn: (s: string) => void) {
  fn('Hello, World');
}

function log(s: string) {
  console.log(s);
}

greet(log);
```

语法 `(s: string) => void` 表达的是 '函数只有一个参数，名为 `s` ，并且类型为 `string` ，没有返回值。就像函数声明一样，如果没有指定参数类型，它将为隐式 `any` 类型。

当然，我们也可以使用类型别名去命名一个函数类型：

```ts
type GreetFn = (s: string) => void;
function greet(fn: GreetFn) {
  // ...
}
```

## 调用签名

在 JavaScript 中，除了可调用之外，函数还可以有属性。然而，函数类型表达式的语法不允许声明属性。如果我们想用属性来描述可调用的东西，我们可以在一个对象类型中写一个调用签名：

```ts
type user = {
  name: string; // 姓名
  (food: number): void; // 进食
};
function foo(user: user) {
  console.log(user.name + ' 吃了 ' + user(3) + ' 包辣条 ');
}
```

注意，与函数类型表达式相比，语法略有不同--在参数列表和返回类型之间使用 `:` 而不是 `=>` 。

## 构造签名

JavaScript 函数也可以用 `new` 操作符来调用。 TypeScript 将这些称为构造函数，因为它们通常会创建一个新的对象。你可以通过在调用签名前面添加 `new` 关键字来写一个构造签名：

```ts
interface Human {
  // xxx
}
type user = {
  new (s: string): Human;
};
function fn(user: user) {
  return new user('xiaoming');
}
```

有些对象，如 JavaScript 的 `Date` 对象，可以在有 `new` 或没有 `new` 的情况下被调用。你可以在同一类型中任意地结合调用和构造签名：

```ts
interface CallOrConstruct {
  new (s: string): Date;
  (n?: number): number;
}
```

## 泛型函数

在写一个函数时，输入的类型与输出的类型有关，或者两个输入的类型以某种方式相关，这是非常常见的。让我们看一下这个函数，作用时返回数组中第一个元素：

```ts
function firstElement(arr: any[]) {
  return arr[0];
}
```

这个函数完成了它的工作，但不幸的是它的返回类型是 `any` 。如果该函数返回数组元素的类型会更好。

在 TypeScript 中，当我们想描述两个值之间的对应关系时，会使用泛型。我们通过在函数签名中声明一个类型参数来做到这一点。

```ts
function firstElement<Type>(arr: Type[]): Type {
  return arr[0];
}
```

通过给这个函数添加一个类型参数 Type，并在两个地方使用它，我们已经在函数的输入（数组）和输出（返回值）之间建立了一个联系。现在当我们调用它时，一个更具体的类型就出来了。

```ts
// s 的类型为 'string'
const s = firstElement(['a', 'b', 'c']);
// n 的类型为 'number'
const n = firstElement([1, 2, 3]);
```

### 推断

请注意，在这个例子中，我们没有必要指定类型。类型是由 TypeScript 自动推断出来的。

我们也可以使用多个类型参数。例如，一个独立版本的 `map` 看起来是这样的：

```ts
function map<Input, Output>(arr: Input[], func: (arg: Input) => Output): Output[] {
  return arr.map(func);
}

// 参数 'n' 类型为 'string'
// 'parsed' 类型为 'number[]'
const parsed = map(['1', '2', '3'], n => parseInt(n));
```

请注意，在这个例子中， TypeScript 可以推断出输入类型参数的类型（来自给定的字符串数组），以及基于函数表达式的返回值（ `number` ）的输出类型参数。

### 约束

我们已经写了一些泛型函数，可以在任何类型的值上工作。有时我们想把两个值联系起来，但只能对某个值的子集进行操作。在这种情况下，我们可以使用一个泛型约束来限制一个参数可以接受的类型。

让我们写一个函数，返回两个值中较长的值。要做到这一点，我们需要一个长度属性，是一个数字。我们通过写一个扩展子句将类型参数限制在这个类型：

```ts
function longest<Type extends { length: number }>(a: Type, b: Type) {
  if (a.length >= b.length) {
    return a;
  } else {
    return b;
  }
}

const longerArray = longest([1, 2], [1, 2, 3]);
// longerArray 类型为 'number[]'

const longerString = longest('alice', 'bob');
// longerString 类型为 'string'

const notOK = longest(10, 100);
// 错误! 数字类型没有 'length' 属性
// 类型 'number' 的参数不能赋给类型 '{ length: number; }' 的参数。
```

在这个例子中，有一些有趣的事情需要注意。我们允许 TypeScript 推断 `longest` 的返回类型。返回类型推断也适用于泛型函数。

因为我们将 `Type` 约束为 ` { length: number }` ，所以我们被允许访问 `a` 和 `b` 参数的 `.length` 属性。如果没有类型约束，我们就不能访问这些属性，因为这些值可能是一些没有 `length` 属性的其他类型。

`longerArray` 和 `longerString` 的类型是根据参数推断出来的。记住，泛型就是把两个或多个具有相同类型的值联系起来

最后，正如我们所希望的那样，对 `longest(10, 100)` 的调用被拒绝了，因为 `number` 类型没有 `.length` 属性。

### 使用泛型约束

这里有一个在使用范型约束时的常见错误：

```ts
function minimumLength<Type extends { length: number }>(obj: Type, minimum: number): Type {
  if (obj.length >= minimum) {
    return obj;
    // 这样返回没问题
  } else {
    return { length: minimum };
    // 不能这么返回!!!
    // 不能将类型 '{ length: number; }' 分配给类型 'Type'.
    // '{ length: number; }' 可赋值给 'Type' 类型的泛型约束，但可以使用约束 '{ length: number; }' 的其他子类型实例化 'Type'。
  }
}
```

看起来这个函数没有问题-- `Type` 被约束为 `{ length: number }` ，而且这个函数要么返回 `Type` ，要么返回一个与该约束相匹配的值。问题是，该函数承诺返回与传入的对象相同的类型，而不仅仅是与约束条件相匹配的一些对象。如果这段代码是合法的，你可以写出完全无法工作的代码：

```ts
const arr = minimumLength([1, 2, 3], 6);
// 'arr' === { length: 6 }
console.log(arr.slice(0));
// 但是这段代码会崩溃，因为返回的是对象，对象并没有 'slice' 方法！
// 而函数本意是返回数组！
```

### 指定泛型参数类型

TypeScript 通常可以推断出泛型函数中的参数类型，但并非总能成功。例如，假设你写了一个函数来合并两个数组。

```ts
function combine<Type>(arr1: Type[], arr2: Type[]): Type[] {
  return arr1.concat(arr2);
}
```

通常情况下，用不匹配的数组调用这个函数将会报错：

```ts
const arr = combine([1, 2, 3], ['hello']);
// 不能将类型 'string' 分配给类型 'number'.
```

然而，如果你打算这样做，你可以手动指定类型：

```ts
const arr = combine<string | number>([1, 2, 3], ['hello']);
```

### 编写高质量泛型函数指南

编写泛型函数很有趣，同时也很容易被类型参数所迷惑。有太多的类型参数或在不需要的地方使用约束，会使类型推断不那么成功，使你的函数的调用者感到沮丧。

#### 规范类型参数

下面是两种看似相似的函数写法：

```ts
function firstElement1<Type>(arr: Type[]) {
  return arr[0];
}

function firstElement2<Type extends any[]>(arr: Type) {
  return arr[0];
}

// a: number (好的方法)
const a = firstElement1([1, 2, 3]);
// b: any (坏的方法)
const b = firstElement2([1, 2, 3]);
```

乍一看，这些可能是相同的，但 `firstElement1` 是写这个函数的一个更好的方法。它的推断返回类型是 `Type` ，但 `firstElement2` 的推断返回类型是 `any` 。

> 记住：尽可能的使用类型参数本身，而不是对其进行约束

#### 更少的类型参数

下面是另一对类似的函数：

```ts
function filter1<Type>(arr: Type[], fn: (arg: Type) => boolean): Type[] {
  return arr.filter(fn);
}

function filter2<Type, Fn extends (arg: Type) => boolean>(arr: Type[], fn: Fn): Type[] {
  return arr.filter(fn);
}
```

我们创建了一个类型参数 `Fn` ，它与上述两个参数并没有什么联系。**千万别干这种蠢事！**因为它意味着想要指定类型参数的调用者必须无缘无故地手动指定一个额外的类型参数。 `Fn` 的出现让函数变得更难阅读和推理，简直 **low** 到地心了

> 记住，尽可能少的使用类型参数

#### 并不是任何情况下都需要类型参数

有些时候我们可能忘了一些函数并不需要泛型：

```ts
function greet<Str extends string>(s: Str) {
  console.log('Hello, ' + s);
}

greet('world');
```

**Don 't do this !!!** 简单点，编码的方式简单点 ✋ 不要多此一举！

```ts
function greet(s: string) {
  console.log('Hello, ' + s);
}
```

记住，类型参数是用来关联多个值的类型的。如果一个类型参数在函数签名中只用了一次，那么类型参数就没有使用的必要。

> 记住，如果一个类型参数只出现在一个地方，请慎重考虑你是否真的需要它

## 可选参数

JavaScript 中的函数经常需要一个可变数量的参数。例如， `number` 的 `toFixed` 方法需要一个可选的 `number` 类型参数：

```ts
function f(n: number) {
  console.log(n.toFixed()); // 0 个参数
  console.log(n.toFixed(3)); // 1 个参数
}
```

在 TypeScript 中，我们可以使用 `?` 操作符来让参数变为可选的：

```ts
function f(x?: number) {
  // ...
}
f(); // OK
f(10); // OK
```

虽然参数被指定为 `number` 类型，但 `x` 参数实际上将具有类型 `number | undefined` ，因为在 JavaScript 中未指定的参数会被赋值为 `undefined` 。

你也可以提供一个参数默认值：

```ts
function f(x = 10) {
  // ...
}
```

现在在 `f` 的主体中， `x` 将具有 `number` 类型，因为任何未定义的参数将被替换为 `10` 。当一个参数是可选的时，调用者可以不传递这个参数：

```ts
declare function f(x?: number): void;
// 下面的调用都ok
f();
f(10);
f(undefined);
```

## 函数重载

一些 JavaScript 函数可以在不同的参数数量和类型中被调用。例如，你可能会写一个函数来产生一个 `Date` ，它需要一个时间戳（一个参数）或一个月/日/年规格（三个参数）。

在 TypeScript 中，我们可以通过编写重载签名来指定一个可以以不同方式调用的函数。要做到这一点，要写一些数量的函数签名（通常是两个或更多），然后是函数的主体。

```ts
// 传入一个时间戳
function makeDate(timestamp: number): Date;
// 传入日月年三个参数
function makeDate(m: number, d: number, y: number): Date;
// 接下来是函数主题，注意可选参数
function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
  if (d !== undefined && y !== undefined) {
    return new Date(y, mOrTimestamp, d);
  } else {
    return new Date(mOrTimestamp);
  }
}
const d1 = makeDate(12345678);
const d2 = makeDate(5, 5, 5);
const d3 = makeDate(1, 3);
// 没有重载需要 2 个参数, 但是有重载需要 1 或者 3 个参数
```

在这个例子中，我们写了两个重载：一个接受一个参数，另一个接受三个参数。这前两个签名被称为重载签名。

然后，我们写了一个具有兼容签名的函数实现。函数有一个实现签名，但这个签名不能被直接调用。即使我们写了一个在必需参数之后有两个可选参数的函数，它也不能用两个参数来调用!

#### 重载签名和实现签名

这是一个常见的混淆点。经常有人会写出这样的代码，却不明白为什么会出现错误：

```ts
function fn(x: string): void;
function fn() {
  // ...
}
// 看起来不传参数也能运行
fn();
// 应有 1 个参数，但是获得 0 个.
```

同样，用于编写函数体的签名不能从外面 '看到'。

> 实现的签名从外面是看不到的。在编写重载函数时，你应该总是在函数的实现上面有两个或多个签名。

实现的签名也必须与重载的签名兼容。例如，这些函数有错误，因为实现签名没有以正确的方式匹配重载。

```ts
function fn(x: string): void;
// 参数类型不正确
// 此重载签名与其实现签名不兼容
function fn(x: boolean) {}
```

```ts
function fn(x: string): string;
// 返回类型不正确
function fn(x: number): boolean;
// 此重载签名与其实现签名不兼容
function fn(x: string | number) {
  return 'oops';
}
```

#### 编写高质量的重载

和泛型一样，在使用函数重载时，有一些准则是你应该遵循的。遵循这些原则将使你的函数更容易调用、理解和实现。

让我们来看看一个返回字符串或数组长度的函数：

```ts
function len(s: string): number;
function len(arr: any[]): number;
function len(x: any) {
  return x.length;
}
```

```ts
len(Math.random() > 0.5 ? 'hello' : [0]);
// 没有与此函数匹配的重载
// 第一个重载, '(s: string): number', 有以下错误.
// 不能将类型 'number[] | 'hello'' 分配给类型 'string'.
// 不能将类型 'number[]' 分配给类型 'string'.
// 第二个重载, '(arr: any[]): number', 有以下错误.
// 不能将类型 'number[] | 'hello'' 分配给类型 'any[]'.
// 不能将类型 'string' 分配给类型 'any[]'.
```

因为两个重载都有相同的参数数和相同的返回类型，我们可以改写一个非重载版本的函数。

```ts
function len(x: any[] | string) {
  return x.length;
}
```

这就好得多了！调用者可以用任何一种值来调用它，而且作为额外的奖励，我们不需要实现一个正确的实现签名。

> 实践中，应该尽可能使用联合类型的参数，而不是重载。

#### 在函数中声明 `this`

TypeScript 将通过代码流分析来推断函数中的 `this` 应该是什么，例如下面的例子。

```ts
const user = {
  id: 123,

  admin: false,
  becomeAdmin: function () {
    this.admin = true;
  },
};
```

函数 `user.becomeAdmin` 内部有一个 `this` ，其指向外部对象 `user` 。大部分情况下你并不需要理会 `this` ，但是也有一些情况下你需要指定 `this` 应该要指向哪个值。在 JavaScript 规范中，你不能指定一个叫 `this` 的参数。但是在 TypeScript 中允许你在参数中声明 `this` 的类型：

```ts
interface DB {
  filterUsers(filter: (this: User) => boolean): User[];
}

const db = getDB();
const admins = db.filterUsers(function (this: User) {
  return this.admin;
});
```

这种模式在回调式 API 中很常见，另一个对象通常控制你的函数何时被调用。注意，你需要使用函数而不是箭头函数来获得这种行为：

```ts
interface DB {
  filterUsers(filter: (this: User) => boolean): User[];
}

const db = getDB();
const admins = db.filterUsers(() => this.admin);
// 箭头函数捕获的是全局的 'this'.
// 元素隐式含有 'any' 类型因为类型 'typeof globalThis'没有索引签名.
```

## 其它你需要知道的类型

有一些额外的类型你会想要认识，它们在处理函数类型时经常出现。像所有的类型一样，你可以在任何地方使用它们，但这些类型在函数的上下文中特别相关。

### `void`

`void` 表示不返回值的函数的返回值。当一个函数没有任何返回语句，或者没有从这些返回语句中返回任何明确的值时，它都是推断出来的类型。

```ts
// The inferred return type is void
function noop() {
  return;
}
```

在 JavaScript 中，一个不返回任何值的函数将隐含地返回 `undefined` 。然而，在 TypeScript 中， `void` 和 `undefined` 是不一样的。在本章末尾有进一步的细节。

> `void` 和 `undefined` 并不完全一样

### `object`

特殊类型的对象指的是任何不是原始类型的值（ `string` , `number` , `boolean` , `symbol` , `null` , `undefined` ）。这与空对象类型 `{ }` 不同，也与全局类型 `Object` 不同。你很可能永远不会使用 `Object` 。

> `object` 并不是 `Object` ，请始终使用 `object`

请注意，在 JavaScript 中，函数是特殊的对象。它们有属性，在它们的原型链中有 `Object.prototype` ，是 `Object` 的实例，你可以对它们调用 `Object.key` ，等等。由于这个原因，函数类型在 TypeScript 中被认为是对象。

`unknown`

`unknown` 代表任何值。这与 `any` 类型相似，但更安全，因为对 `unknown` 做任何事情都是不合法的。

```ts
function f1(a: any) {
  a.b(); // OK
}
function f2(a: unknown) {
  a.b();
  // 类型 'unknown' 上不存在属性 'b'
}
```

这在描述函数类型时很有用，因为你可以描述接受任何值的函数，而不需要在函数体中有任何值。

反之，你可以描述一个返回未知类型的值的函数。

```ts
function safeParse(s: string): unknown {
  return JSON.parse(s);
}

// obj: unknown
const obj = safeParse(someRandomString);
```

`never`

一些函数将永远无法返回值：

```ts
function fail(msg: string): never {
  throw new Error(msg);
}
```

`never` 类型表示永远不会被观察到的值。在一个返回类型中，这意味着函数抛出一个异常或终止程序的执行。

`never` 也用于在 TypeScript 确定一个联合类型中没有任何匹配的时候：

```ts
function fn(x: string | number) {
  if (typeof x === 'string') {
    // do sth
  } else if (typeof x === 'number') {
    // do sth else
  } else {
    x; // x: never
  }
}
```

`Function`

全局性的 Function 类型描述了诸如 `bind` 、 `call` 、 `apply` 和其他存在于 JavaScript 中所有函数值的属性。它还有一个特殊的属性，即 `Function` 类型的值总是可以被调用；这些调用将返回 `any` :

```ts
function doSomething(f: Function) {
  f(1, 2, 3);
}
```

这是一个无类型的函数调用，一般来说最好避免，因为返回 `any` 类型并不安全。

如果你需要接受一个任意的函数，但不打算调用它，一般来说， `() => void` 的类型比较安全。

## 参数展开

### 在定义时展开

除了使用可选参数或重载来制作可以接受各种固定参数数量的函数之外，我们还可以使用参数展开符来定义接受无限制数量参数的函数。

参数展开使用 `...` 语法，并且需要出现在所有其他参数之后：

```ts
function multiply(n: number, ...m: number[]) {
  return m.map(x => n * x);
}
// 'a' === [10, 20, 30, 40]
const a = multiply(10, 1, 2, 3, 4);
```

在 TypeScript 中，这些参数的类型注解是隐式 `any[]` ，而不是 `any` ，而且给出的任何类型注解必须是 `Array<T>` 或 `T[]` 的形式，或者是元组类型（我们将在后面学习）。

### 在调用时展开

相反地，我们可以使用参数展开语法从一个数组中提供数量可变的参数。例如，数组的 push 方法需要任意数量的参数。

```ts
const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
arr1.push(...arr2);
```

请注意，一般来说， TypeScript 并不假定数组是不可变的。这可能会导致一些令人惊讶的行为。

```ts
// args 推断类型为 number[] -- '一个只包含任意个数字的数组',
// 而不仅仅限定为2个数字的元祖
const args = [8, 5];
const angle = Math.atan2(...args);
// Math.atan2的类型为 (y: number, x: number): number
// 扩张参数必须具有元组类型或传递给 rest 参数。
// 这里将会报错
```

这种情况的最佳解决方案取决于你的代码，但一般来说，使用 `const` 上下文是最直接的解决方案。

```ts
// 使用 const 推断为长度为 2 的元祖
const args = [8, 5] as const;
// OK
const angle = Math.atan2(...args);
```

## 参数解构

你可以使用参数重构来方便地将--作为参数提供的对象--解压到函数主体的一个或多个局部变量中。在 JavaScript 中，它看起来像这样：

```ts
function sum({ a, b, c }) {
  console.log(a + b + c);
}
sum({ a: 10, b: 3, c: 9 });
```

对象的类型注解在去结构化的语法之后。

```ts
function sum({ a, b, c }: { a: number; b: number; c: number }) {
  console.log(a + b + c);
}
```

这看起来有点啰嗦，但你也可以在这里使用一个命名的类型。

```ts
// Same as prior example
type ABC = { a: number; b: number; c: number };
function sum({ a, b, c }: ABC) {
  console.log(a + b + c);
}
```

## 函数的可分配性

### 返回类型 `void`

函数的 `void` 返回类型可以产生一些不寻常的，但却是预期的行为。

返回类型为 `void` 的上下文类型并不强迫函数不返回值。也就是说，一个具有 `void` 返回类型的上下文函数类型（ `type vf = () => void` ），在实现时，可以返回任何其他的值，但它会被忽略。

因此，以下 `()=> void` 类型的实现是有效的：

```ts
type voidFn = () => void;

const f1: voidFn = () => {
  return true;
};

const f2: voidFn = () => true;

const f3: voidFn = function () {
  return true;
};
```

而当这些函数之一的返回值被分配给另一个变量时，它将保留 `void` 的类型。

```ts
const v1 = f1();
// v1: void
const v2 = f2();
// v2: void
const v3 = f3();
// v3: void
```

这种行为的存在使得下面的代码是有效的，即使 `Array.prototype.push` 返回一个数字，而 `Array.prototype.forEach` 方法期望一个返回类型为 `void` 的函数：

```ts
const src = [1, 2, 3];
const arr = [0];

src.forEach(i => arr.push(i));
```

还有一个需要注意的特殊情况，当一个具名函数（与匿名函数相反）定义有一个 `void` 的返回类型时，该函数必须不返回任何值：

```ts
function f2(): void {
  // 不能将类型 'boolean' 分配给类型 'void'
  return true;
}
```

# 对象类型

在 JavaScript 中，我们基本都是通过对象来组织和传递数据的。在 TypeScript 中，我们通过 `object` 类型来表示这些对象。

正如我们所见，它们可以是匿名的：

```ts
function greet(user: { name: string; age: number }) {
  return 'Hello ' + user.name;
}
```

或者可以通过使用一个接口来命名它们：

```ts
interface user {
  name: string;
  age: number;
}

function greet(user: user) {
  return 'Hello ' + user.name;
}
```

或者使用类型别名：

```ts
type user = {
  name: string;
  age: number;
};

function greet(user: user) {
  return 'Hello ' + user.name;
}
```

在上面的三个例子中，我们写了一些函数，这些函数接收一个必须包含属性--名字（必须是一个 `string` ）和年龄（必须是一个 `number` ）的对象。

## 属性修饰符

一个对象类型中的每个属性都可以指定几件事：类型、属性是否是可选的，以及属性是否是只读的。

### 可选属性

很多时候，我们会发现自己处理的对象可能有一个属性设置。在这些情况下，我们可以在这些属性的名称后面加上一个问号（ `?` ），将其标记为可选属性。

```ts
interface user {
  name: string;
  age?: number;
  sex?: string;
}

function logUser(user: user) {
  console.log(user);
}

logUser({ name: 'xiaoming' });
logUser({ name: 'xiaoming', age: 17 });
logUser({ name: 'xiaoming', sex: 'male' });
logUser({ name: 'xiaoming', age: 17, sex: 'male' });
```

在这个例子中， `age` 和 `sex` 都被认为是可选的。我们可以选择提供它们中的任意一个，所以以上对 `logUser` 的每个调用都是有效的。所有的可选性实际上是说，如果属性被设置，它最好有一个特定的类型。

我们也可以从通过访问这些属性，来读取属性对应的值--但当我们在 `strictNullChecks` 下读取时， TypeScript 会告诉我们它们有可能是 `undefined` 。

```ts
function logUser(user: user) {
  let age = user.age;
  // (property) user.age?: number | undefined
  let sex = user.sex;
  // (property) user.sex?: string | undefined
  // ...
}
```

在 JavaScript 中，即使该属性从未被定义过，我们仍然可以访问它--未定义的属性的值将会是 `undefined` 。我们可以为未定义的值设置默认值：

```ts
function logUser({ name: string, age = 17, sex = 'male' }: user) {
  // ...
}
```

在这里，我们为 `logUser` 的参数使用了一个解构模式，并为 `age` 和 `sex` 提供了参数默认值。现在 `age` 和 `sex` 都肯定存在于 `logUser` 的主体中，但对于 `logUser` 的任何调用者来说是可选的。

### 只读属性

对于 TypeScript ，属性也可以被标记为只读。虽然它 **不会** 在运行时改变任何行为，但在类型检查期间，一个标记为只读的属性不能被写入

> 虽然会在 TypeScript 这里报错，但是运行时该写入还是会写入 😓

```ts
interface user {
  readonly name: string;
}

function setName(user: user) {
  // 可以读取 'obj.prop'.
  console.log(`user.name is '${user.name}'.`);

  // 但是我们不能对其重新赋值.
  user.name = 'xiaohong';
  // 不能对 'name' 重新赋值因为它是只读的.
}
```

使用 `readonly` 修饰符并不一定意味着一个值是完全不可改变的--或者换句话说，不可改变的仅仅是变量指向的内存地址。

### 索引签名

有时你并不提前知道一个类型的所有属性名称，但你知道值的形状。

在这些情况下，你可以使用一个索引签名来描述可能的值的类型，比如说：

```ts
interface StringArray {
  [index: number]: string;
}
```

索引签名的属性类型必须是 `string` 或 `number` 。

虽然字符串索引签名是描述 '字典 '模式的一种强大方式，但它也强制要求所有的属性与它们的返回类型相匹配。在下面的例子中， `name` 的类型与字符串索引的类型不匹配，类型检查器会给出一个错误。

```ts
interface NumberDictionary {
  [index: string]: number;
  length: number; // ok
  name: string;
  // 类型 string 的属性 name 不能赋给字符串索引类型 number
}
```

然而，如果索引签名是一个联合类型，联合类型之一的子类型是可以的：

```ts
interface NumberOrStringDictionary {
  [index: string]: number | string;
  length: number; // ok, length is a number
  name: string; // ok, name is a string
}
```

最后，你可以使索引签名为只读，以防止对其索引的赋值。

```ts
interface ReadonlyStringArray {
 readonly [index: number]: string;
}

let myArray: ReadonlyStringArray = getReadOnlyStringArray();
myArray[2] = 'Mallory';
Index signature in type 'ReadonlyStringArray' only permits reading.
```

你不能设置 `myArray[2]` ，因为这个索引签名是只读的。

## 扩展类型

有一些类型可能是其他类型的更具体的版本，这是很常见的。例如，我们可能有一个 `BasicAddress` 类型，描述在美国发送信件和包裹所需的字段。

```ts
interface BasicAddress {
  name?: string; // 名称
  street: string; // 街道
  city: string; // 城市
  country: string; // 国家
  postalCode: string; //邮编
}
```

在某些情况下，这就足够了，但是如果一个地址的建筑物有多个单元，那么地址往往有一个单元号与之相关。我们就可以描述一个新接口 `AddressWithUnit` 。

```ts
interface AddressWithUnit {
  name?: string; // 名称
  unit: string; // 单元号
  street: string; // 街道
  city: string; // 城市
  country: string; // 国家
  postalCode: string; // 邮编
}
```

这样就差不多了。但这里的缺点是，当我们的变化是纯粹的加法时，我们不得不重复 `BasicAddress` 的所有其他字段。作为替代，我们可以扩展原始的 `BasicAddress` 类型，只需添加 `AddressWithUnit` 特有的新字段。

```ts
interface BasicAddress {
  name?: string; // 名称
  street: string; // 街道
  city: string; // 城市
  country: string; // 国家
  postalCode: string; // 邮编
}

interface AddressWithUnit extends BasicAddress {
  unit: string; // 单元号
}
```

接口上的 `extends` 关键字允许我们有效地从其他命名的类型中复制成员，并添加我们想要的任何新成员。例如， `AddressWithUnit` 不需要重复 `street` 属性，而且因为 `street` 源于 `BasicAddress` ，读者会知道这两种类型在某种程度上是相关的。

接口也可以一次 `extends` 多个类型：

```ts
interface Colorful {
  color: string;
}

interface Circle {
  radius: number;
}

interface ColorfulCircle extends Colorful, Circle {}

const cc: ColorfulCircle = {
  color: 'red',
  radius: 42,
};
```

## 交叉类型

接口允许我们通过扩展其他类型建立起新的类型。 TypeScript 提供了另一种结构，称为交叉类型，主要用于组合现有的对象类型。

交叉类型是用 `&` 操作符定义的。

```ts
interface Colorful {
  color: string;
}
interface Circle {
  radius: number;
}

type ColorfulCircle = Colorful & Circle;
```

在这里，我们将 `Colorful` 和 `Circle` 相交，产生了一个新的类型，它拥有 `Colorful` 和 `Circle` 的所有成员。

```ts
function draw(circle: Colorful & Circle) {
  console.log(`Color : ${circle.color}`);
  console.log(`Radius : ${circle.radius}`);
}

// 正常调用
draw({ color: 'blue', radius: 42 });

// 出错
draw({ color: 'blue', raidus: 42 });
// raidus 拼写错误
```

## 接口 VS 交叉类型

我们刚刚看了两种组合类型的方法，它们很相似，但实际上有细微的不同。对于接口，我们可以使用 `extends` 子句来扩展其他类型，而对于交叉类型，我们也可以做类似的事情，并用类型别名来命名结果。两者之间的主要区别在于如何处理冲突，这种区别通常是你在接口和交叉类型的类型别名之间选择一个的主要原因之一。

## 泛型对象类型

让我们想象一下，一个可以包含任何数值的 `Box` 类型-- `string` 、 `number` 、 `Giraffe` ，随便什么类型：

```ts
interface Box {
  contents: any;
}
```

现在，内容属性的类型是 `any` ，虽然又不是不能用 😇，但是我们还是需要改写它 🧐。

我们可以使用 `unknown` 替代之。但这意味着就算我们早已经知道 `content` 类型的情况下，我们仍然需要校验其类型，或者使用容易出错的类型断言：

```ts
interface Box {
  contents: unknown;
}

let x: Box = {
  contents: 'hello world',
};

//检查 'x.contents'
if (typeof x.contents === 'string') {
  console.log(x.contents.toLowerCase());
}

// 或者使用类型断言
console.log((x.contents as string).toLowerCase());
```

一种安全的方法是为每一种类型的内容搭建不同的 `Box` 类型。

```ts
interface NumberBox {
  contents: number;
}

interface StringBox {
  contents: string;
}

interface BooleanBox {
  contents: boolean;
}
```

但这意味着我们必须创建不同的函数，或函数的重载，以对这些类型进行操作。

```ts
function setContents(box: StringBox, newContents: string): void;
function setContents(box: NumberBox, newContents: number): void;
function setContents(box: BooleanBox, newContents: boolean): void;
function setContents(box: { contents: any }, newContents: any) {
  box.contents = newContents;
}
```

这是一个繁琐的过程。此外，我们以后可能需要引入新的类型和重载。这是令人沮丧的，因为我们的 `Box` 类型和重载实际上都是相同的。

作为替代，我们可以创建一个泛型的 `Box` 类型，声明一个类型参数。

```ts
interface Box<Type> {
  contents: Type;
}
```

你可以把这句话理解为：'一个类型的盒子是其内容具有类型的东西'。以后，当我们引用 `Box` 时，我们必须给一个类型参数来代替 `Type` 。

```ts
let box: Box<string>;
```

把 `Box` 想象成一个真实类型的模板，其中 `Type` 是一个占位符，会被替换成其他类型。当 TypeScript 看到 `Box<string>` 时，它将在每个实例中，用 `string` 替换 `Box<Type>` 中 `Type` ，并最终以 `{ contents: string }` 这样的方式工作。换句话说， `Box<string>` 和我们之前的 `StringBox` 工作起来是一样的。

```ts
interface Box<Type> {
  contents: Type;
}
interface StringBox {
  contents: string;
}

let boxA: Box<string> = { contents: 'hello' };
console.log(boxA.contents);
// (property) Box<string>.contents: string

let boxB: StringBox = { contents: 'world' };
console.log(boxB.contents);
// (property) StringBox.contents: string
```

`Box` 是可复用的，因为 `Type` 可以用任何类型来代替。这意味着当我们需要一个新类型的 `Box` 时，我们无需声明一个新的 `Box` 类型（尽管如果我们想的话，我们当然可以）。

```ts
interface Box<Type> {
  contents: Type;
}

interface Apple {
  // ....
}

// Same as '{ contents: Apple }'.
type AppleBox = Box<Apple>;
```

这也意味着我们可以完全避免重载，而是改用泛型函数。

```ts
function setContents<Type>(box: Box<Type>, newContents: Type) {
  box.contents = newContents;
}
```

值得注意的是，类型别名也可以是泛型的。我们可以定义我们新的 `Box<Type>` 接口，就像这样：

```ts
interface Box<Type> {
  contents: Type;
}
```

使用类型别名代替：

```ts
type Box<Type> = {
  contents: Type;
};
```

由于类型别名与接口不同，它不仅可以描述对象类型，我们还可以用它来编写其他类型的泛型辅助类型。

```ts
type OrNull<Type> = Type | null;

type OneOrMany<Type> = Type | Type[];

type OneOrManyOrNull<Type> = OrNull<OneOrMany<Type>>;
// type OneOrManyOrNull<Type> = OneOrMany<Type> | null

type OneOrManyOrNullStrings = OneOrManyOrNull<string>;
// type OneOrManyOrNullStrings = OneOrMany<string> | null
```

我们稍后再讨论类型别名。

### 数组类型

泛型对象类型通常是某种容器类型，它的工作与它们所包含的元素类型无关。数据结构以这种方式工作是很理想的，这样它们就可以在不同的数据类型中重复使用。

事实证明，在这本手册中，我们一直在使用这样一种类型： `Array` 类型。每当我们写出 `number[]` 或 `string[]` 这样的类型时，这实际上只是 `Array<number>` 和 `Array<string>` 的简写。事实上， `Array` 本身也是一个泛型类型：

```ts
interface Array<Type> {
  length: number;
  pop(): Type | undefined;
  push(...items: Type[]): number;
  // ...
}
```

现代 JavaScript 还提供了其他泛型的数据结构，比如 `Map<K, V>` , `Set<T>` , 和 `Promise<T>` 。这实际上意味着，由于 `Map` 、 `Set` 和 `Promise` 的行为方式，它们可以与任何类型的集合一起工作。

### 只读类型的数组

`ReadonlyArray` 是一个特殊的类型，用来描述不应该被改变的数组。

```ts
function doStuff(values: ReadonlyArray<string>) {
  // 我们可以读取 'values'...
  const copy = values.slice();
  console.log(`The first value is ${values[0]}`);

  // ...但是不能写入 'values'.
  values.push('hello!');
  // 类型 readonly string[] 上不存在属性 push。
}
```

与 `Array` 不同，没有一个我们可以使用的 `ReadonlyArray` 构造函数。

```ts
new ReadonlyArray('red', 'green', 'blue');
// 'ReadonlyArray' 只为被作为类型而不是作为值使用
```

作为替代，我们可以将普通的 `Arrays` 分配给 `ReadonlyArrays` :

```ts
const roArray: ReadonlyArray<string> = ['red', 'green', 'blue'];
```

正如 TypeScript 为 `Array<Type>` 提供了 `Type[]` 的精简语法一样，它也为 `ReadonlyArray<Type>` 提供了 `readonly Type[]` 的精简语法：

```ts
function doStuff(values: readonly string[]) {
  // 我们可以从 'values' 读取...
  const copy = values.slice();
  console.log(`The first value is ${values[0]}`);

  // ...但是我们不能改变 'values'.
  values.push('hello!');
  // 属性 'push' 不存在于类型 'readonly string[]'.
}
```

最后要注意的是，只读数组和普通数组并不能互相赋值：

```ts
let x: readonly string[] = [];
let y: string[] = [];

x = y;
y = x;
// 类型 'readonly string[]' 为 'readonly'，不能分配给可变类型 'string[]'。
```

### 元祖类型

元组类型是另一种数组类型，它确切地知道它包含多少个元素，以及它在特定位置包具体是哪种类型。

```ts
type StringNumberPair = [string, number];
```

这里， `StringNumberPair` 是一个 `string` 和 `number` 的元组类型。描述了索引为 `0` 的元素为 `string` 类型，索引为 `1` 的元素为 `number` 类型的数组。

```ts
function doSomething(pair: [string, number]) {
  const a = pair[0];
  // const a: string

  const b = pair[1];
  // const b: number
  // ...
}

doSomething(['hello', 42]);
```

如果我们试图访问超过元素的数量的索引，我们会得到一个错误。

```ts
function doSomething(pair: [string, number]) {
  // ...

  const c = pair[2];
  // 长度为2的元祖类型 '[string, number]' 在索引为 '2' 处没有元素.
}
```

我们还可以使用 JavaScript 的数组解构来对元祖进行解构。

```ts
function doSomething(stringHash: [string, number]) {
  const [inputString, hash] = stringHash;

  console.log(inputString);
  // const inputString: string

  console.log(hash);
  // const hash: number
}
```

元祖类型还能使用特定的接口形状描述，在特定的索引上声明属性，并且用数字字面量类型声明 `length` :

```ts
interface StringNumberPair {
  length: 2;
  0: string;
  1: number;

  // Other 'Array<string | number>' members...
  slice(start?: number, end?: number): Array<string | number>;
}
```

另一件你可能感兴趣的事情是，元组中可以存在可选的元素（使用 `?` ）。可选的元组元素只能出现在最后，而且也会影响到 `length` 的类型。

```ts
type Either2dOr3d = [number, number, number?];

function setCoordinate(coord: Either2dOr3d) {
  const [x, y, z] = coord;
  // const z: number | undefined

  console.log(`Provided coordinates had ${coord.length} dimensions`);
  // (property) length: 2 | 3
}
```

元祖也可以有展开元素，这些元素必须是数组/元祖类型。

```ts
type StringNumberBooleans = [string, number, ...boolean[]];
type StringBooleansNumber = [string, ...boolean[], number];
type BooleansStringNumber = [...boolean[], string, number];
```

一个有展开元素的元组没有 `length` 属性--它只有一组不同位置的已知元素。

```ts
const a: StringNumberBooleans = ['hello', 1];
const b: StringNumberBooleans = ['beautiful', 2, true];
const c: StringNumberBooleans = ['world', 3, true, false, true, false, true];
```

为什么可选元素和其余元素可能是有用的？嗯，它允许 TypeScript 将元祖与参数列表相对应。元祖类型可以 `rest参数` `arguments` 中使用。如下：

```ts
function readButtonInput(...args: [string, number, ...boolean[]]) {
  const [name, version, ...input] = args;
  // ...
}
```

基本上等同于：

```ts
function readButtonInput(name: string, version: number, ...input: boolean[]) {
  // ...
}
```

当你想用一个展开参数接受可变数量的参数，并且你需要一个最小的元素数量，但你不想引入中间变量时，这很方便。

### 只读元祖类型

元组类型还有只读类型，在其前面加一个 `readonly` 修饰符即可

```ts
function doSomething(pair: readonly [string, number]) {
  // ...
}
```

正如预期的那样，在 TypeScript 中不允许向只读元组的任何属性写入。

```ts
function doSomething(pair: readonly [string, number]) {
  pair[0] = 'hello!';
  // Cannot assign to '0' because it is a read-only property.
}
```

在大多数代码中，元组往往被创建并不被修改，所以在可能的情况下，将类型注释为只读元组是一个很好的默认行为。给数组加上 `const` 断言也是个不错的习惯，因为带有 `const` 断言的数组将被推断为只读元组类型。

```ts
let point = [3, 4] as const;

function distanceFromOrigin([x, y]: [number, number]) {
  return Math.sqrt(x ** 2 + y ** 2);
}

distanceFromOrigin(point);
// 类型 'readonly [3, 4]' 是只读的，不能被重新赋值为类型 '[number, number]'.
```

# 从已有类型中创造其它类型

TypeScript 的类型系统非常强大，因为它允许使用其他类型创建新类型。

## 泛型

软件工程的一个主要部分是建立组件，这些组件不仅有定义明确和一致的 API，而且还可以重复使用。在像 **C#** 和 **Java** 这样的语言中，泛型是创建可重用组件的主要工具之一，也就是说，能够创建一个能够在各种类型上工作的组件，而不是单一的类型。这使得用户可以消费这些组件并使用他们自己的类型。

### 泛型之 Hello World

首先，让我们创建一个泛型的 'Hello World': `identity` 函数。它将返回传入的任何值。

如果没有泛型，我们将不得不给 `identity` 函数一个特定的类型：

```ts
function identity(arg: number): number {
  return arg;
}
```

或者，我们可以用 `any` 类型来描述 `identity` 函数：

```ts
function identity(arg: any): any {
  return arg;
}
```

但是使用 `any` 将不会像我们预期的那样，因为这意味着输入和输出将并不一定是同一种类型。

作为替代，我们需要一种方法来捕获参数的类型，以便我们也可以用它来表示返回的类型。在这里，我们将使用一个类型变量，这是一种特殊的变量，对类型而不是数值起作用：

```ts
function identity<Type>(arg: Type): Type {
  return arg;
}
```

我们现在已经在 `indetity` 函数中添加了一个类型变量 `Type` 。这个 `Type` 允许我们捕获用户提供的类型（例如 `number` ），这样我们就可以在以后使用这些信息。这里，我们再次使用 `Type` 作为返回类型。经过检查，我们现在可以看到参数和返回类型使用的是相同的类型。这使得我们可以将类型信息从函数的一侧输入，然后从另一侧输出。

当我们写好了泛型 `identity` 函数，我们可以用两种方式调用它。第一种方式是将所有的参数，包括类型参数，都传递给函数。

```ts
let output = identity<string>('myString');
// let output: string
```

这里我们明确地将 `Type` 设置为 `string` ，作为函数调用的参数之一，并用 `<>` 包裹而不是 `()`

第二种方式可能也是最常见的。这里我们使用类型参数推断--也就是说，我们希望编译器根据我们传入的参数的类型，自动为我们设置 `Type` 的值。

```ts
let output = identity('myString');
// let output: string
```

注意，我们不必明确地在角括号 `<>` 中传递类型。编译器只是查看了值 `'myString'` ，并将 `Type` 设置为其类型。虽然类型参数推断是一个有用的工具，可以使代码更短、更易读，但当在更复杂的类型中，编译器无法推断类型时，你可能需要像我们在前面的例子中那样明确地传入类型参数。

### 使用泛型类型变量

当你开始使用泛型时，你会注意到，当你创建像 `identity` 这样的泛型函数时，编译器会强制要求你正确使用函数主体中的所有泛型参数。

让我们来看看我们之前的 `identity` 函数：

```ts
function identity<Type>(arg: Type): Type {
  return arg;
}
```

如果我们想在每次调用时将参数 `arg` 的长度记录到控制台，该怎么办？我们可能会这么写：

```ts
function loggingIdentity<Type>(arg: Type): Type {
  console.log(arg.length);
  // 属性 'length' 不存在于类型 'Type'.
  return arg;
}
```

当我们这样做时，编译器会给我们一个错误，说我们在使用 `arg` 的 `.length` 属性，但我们没有声明 `arg` 有这个属性。例如我们可以传入一个数字，而数字并不会存在 `.length` 属性。

比方说，我们实际上是想让这个函数在 `Type` 的数组上工作，而不是直接在 `Type` 上工作。既然我们在处理数组，那么 `.length` 属性应该是存在的。我们可以像创建其他类型的数组那样来描述它。

```ts
function loggingIdentity<Type>(arg: Type[]): Type[] {
  console.log(arg.length);
  return arg;
}
```

你可以把 `loggingIdentity` 的类型理解为 --'泛型函数 `loggingIdentity` 接收一个类型参数 `Type` 和一个参数 `arg` ， `arg` 是一个 `Type` 数组，并返回一个 `Type` 数组。' 如果我们传入一个 `number` 数组，我们会得到一个 `number` 数组，因为 `Type` 会绑定为 `number` 。这允许我们使用我们的泛型类型变量 `Type` 作为我们正在处理的类型的一部分，而不是整个类型，给我们更大的灵活性。

### 泛型类型

在前几节中，我们创建了在一系列类型上工作的泛型 `identity` 函数。在本节中，我们将探讨函数本身的类型以及如何创建泛型接口。

泛型函数的类型与非泛型函数的类型一样，类型参数列在前面，与函数声明类似。

```ts
function identity<Type>(arg: Type): Type {
  return arg;
}

let myIdentity: <Type>(arg: Type) => Type = identity;
```

我们也可以为类型中的泛型类型参数使用一个不同的名字，只要类型变量的数量和类型变量的使用方式一致。

```ts
function identity<Type>(arg: Type): Type {
  return arg;
}

let myIdentity: <Input>(arg: Input) => Input = identity;
```

我们也可以把泛型写成一个对象字面类型的调用签名：

```ts
function identity<Type>(arg: Type): Type {
  return arg;
}

let myIdentity: { <Type>(arg: Type): Type } = identity;
```

这让我们开始编写我们的第一个泛型接口。让我们把前面例子中的对象字面量移到一个接口中：

```ts
interface GenericIdentityFn {
  <Type>(arg: Type): Type;
}

function identity<Type>(arg: Type): Type {
  return arg;
}

let myIdentity: GenericIdentityFn = identity;
```

在一个类似的例子中，我们可能想把泛型参数移到整个接口的参数上。这可以让我们看到我们的泛型是什么类型（例如， `Dictionary<string>` 而不是仅仅 `Dictionary` ）。这使得类型参数对接口的所有其他成员可见。

```ts
interface GenericIdentityFn<Type> {
  (arg: Type): Type;
}

function identity<Type>(arg: Type): Type {
  return arg;
}

let myIdentity: GenericIdentityFn<number> = identity;
```

请注意，我们的例子已经改变了，变成了稍微不同的东西。我们现在不是描述一个泛型函数，而是有一个非泛型的函数签名，它是泛型类型的一部分。当我们使用 `GenericIdentityFn` 时，我们现在还需要指定相应的类型参数（这里是： `number` ），有效地锁定了底层调用签名将使用什么。了解什么时候把类型参数直接放在调用签名上，什么时候把它放在接口本身，将有助于描述一个类型的哪些方面是泛型的。

除了泛型接口之外，我们还可以创建泛型类。注意，不能创建泛型枚举和泛型命名空间。

### 泛型类

泛型类的形状与泛型接口相似。泛型类在类的名字后面有一个角括号 `<>` 中的泛型参数列表。

```ts
class GenericNumber<NumType> {
  zeroValue: NumType;
  add: (x: NumType, y: NumType) => NumType;
}

let myGenericNumber = new GenericNumber<number>();
myGenericNumber.zeroValue = 0;
myGenericNumber.add = function (x, y) {
  return x + y;
};
```

这是对 `GenericNumber` 类相当直白的使用，但你可能已经注意到，没有任何东西限制它只能使用数字类型。我们本可以使用 `string` 或更复杂的对象。

```ts
let stringNumeric = new GenericNumber<string>();
stringNumeric.zeroValue = '';
stringNumeric.add = function (x, y) {
  return x + y;
};

console.log(stringNumeric.add(stringNumeric.zeroValue, 'test'));
```

就像接口一样，把类型参数放在类本身，可以让我们确保类的所有属性都与相同的类型一起工作。

正如我们在关于类的章节中所说的，一个类的类型有两个方面：静态方面和实例方面。泛型类只在其实例侧而非静态侧具有泛型性，所以当使用类时，静态成员不能使用该类的类型参数。

### 泛型约束

如果你还记得前面的例子，你有时可能想写一个泛型函数，在一组类型上工作，而你对这组类型会有什么能力有一定了解。在我们的 `loggingIdentity` 例子中，我们希望能够访问 `arg` 的 `.length` 属性，但是编译器无法证明每个类型都有一个 `.length` 属性，所以它警告我们不能做这个假设。

```ts
function loggingIdentity<Type>(arg: Type): Type {
  console.log(arg.length);
  // 类型 'Type' 上不存在属性 'length'.
  return arg;
}
```

所以，这里我们需要约束传入的 `arg` 必须含有 `length` 属性。比如我们创建一个接口，它有一个单一的 `.length` 属性，然后我们将使用这个接口和 `extends` 关键字来表示我们的约束条件：

```ts
interface Lengthwise {
  length: number;
}

function loggingIdentity<Type extends Lengthwise>(arg: Type): Type {
  console.log(arg.length); // 因为我们知道 arg 含有属性 .length , 所以这里不会再报错
  return arg;
}
```

因为泛型函数现在被约束了，它将不再对 `any` 和所有的类型起作用：

```ts
loggingIdentity(3);
// 类型 'number' 的参数不能赋给类型 'Lengthwise' 的参数。
```

作为替代，我们需要传入其类型包含所有所需属性的值：

```ts
loggingIdentity({ length: 10, value: 3 });
```

### 在泛型约束内使用类型参数

你可以声明一个受另一个类型参数约束的类型参数。例如，在这里我们想从一个给定名称的对象中获取一个属性。我们想确保我们不会意外地传入一个不存在于 `obj` 上的属性，所以我们要在这两种类型之间放置一个约束条件。

```ts
function getProperty<Type, Key extends keyof Type>(obj: Type, key: Key) {
  return obj[key];
}

let x = { a: 1, b: 2, c: 3, d: 4 };

getProperty(x, 'a');
getProperty(x, 'm');
// 类型 ''m'' 的参数不能赋值给类型 ''a' | 'b' | 'c' | 'd'' 的参数.
```

### 在泛型中使用 class

在 TypeScript 中使用泛型创建工厂时，有必要通过其构造函数来引用类的类型。比如说：

```ts
function create<Type>(c: { new (): Type }): Type {
  return new c();
}
```

一个更高级的例子使用原型属性来推断和约束类类型的构造函数和实例方之间的关系。

```ts
class BeeKeeper {
  hasMask: boolean = true;
}

class ZooKeeper {
  nametag: string = 'Mikle';
}

class Animal {
  numLegs: number = 4;
}

class Bee extends Animal {
  keeper: BeeKeeper = new BeeKeeper();
}

class Lion extends Animal {
  keeper: ZooKeeper = new ZooKeeper();
}

function createInstance<A extends Animal>(c: new () => A): A {
  return new c();
}

createInstance(Lion).keeper.nametag;
createInstance(Bee).keeper.hasMask;
```

这种模式被用来为 `mixins` 设计模式提供支持。

## Keyof 操作符

`keyof` 运算符接收一个对象类型，并返回由其键名组成的联合类型：

```ts
type Point = { x: number; y: number };
type P = keyof Point;
```

如果该类型有一个字符串或数字索引签名， `keyof` 将返回这些类型：

```ts
type Arrayish = { [n: number]: unknown };
type A = keyof Arrayish;
// type A = number

type Mapish = { [k: string]: boolean };
type M = keyof Mapish;
// type M = string | number
```

注意，在这个例子中， `M` 是 `string | number` --这是因为 JavaScript 对象的键总是被强制为字符串，所以 `obj[0]` 总是与 `obj['0']` 相同。

`keyof` 类型在与映射类型结合时变得特别有用，我们将在后面进一步了解。

## Typeof 操作符

JavaScript 已经有一个 `typeof` 操作符，你可以在表达式上下文中使用：

```ts
// Prints 'string'
console.log(typeof 'Hello world');
```

TypeScript 添加了一个 `typeof` 操作符，你可以在类型上下文中使用它来引用一个变量或属性的类型。

```ts
let s = 'hello';
let n: typeof s;
// let n: string
```

这对基本类型来说不是很有用，但结合其他类型操作符，你可以使用 `typeof` 来方便地表达许多模式。对于一个例子，让我们先看看预定义的类型 `ReturnType<T>` 。它接收一个函数类型并产生其返回类型。

```ts
type Predicate = (x: unknown) => boolean;
type K = ReturnType<Predicate>;
// type K = boolean
```

如果我们试图在一个函数名上使用 `ReturnType` ，我们会看到一个指示性的错误。

```ts
function f() {
  return { x: 10, y: 3 };
}
type P = ReturnType<f>;
// 'f' 被推断为值, 但是在这里被用作类型. 你是说 'typeof f'?
```

请记住，值和类型并不是一回事。为了指代值 `f` 的类型，我们使用 `typeof` :

```ts
function f() {
  return { x: 10, y: 3 };
}
type P = ReturnType<typeof f>;
// type P = {
// x: number;
// y: number;
// }
```

#### 限制

TypeScript 故意限制了你可以使用 `typeof` 的表达式种类。

具体来说，只有在标识符（即变量名）或其属性上使用 `typeof` 是合法的。这有助于避免混乱的陷阱，即编写你认为是在执行的代码，但其实不是。

```ts
// 本义是指 ReturnType<typeof msgbox>
let shouldContinue: typeof msgbox('Are you sure you want to continue?');
// ',' expected.
```

## 索引访问类型

我们可以使用一个索引访问类型来查询另一个类型上的特定属性。

```ts
type Person = { age: number; name: string; alive: boolean };
type Age = Person['age'];
// type Age = number
```

索引类型本身就是一个类型，所以我们可以完全使用 `unions` 、 `keyof` 或者其他类型。

```ts
type I1 = Person['age' | 'name'];
// type I1 = string | number

type I2 = Person[keyof Person];
// type I2 = string | number | boolean

type AliveOrName = 'alive' | 'name';
type I3 = Person[AliveOrName];
// type I3 = string | boolean
```

如果你试图索引一个不存在的属性，你将会看到一个错误：

```ts
type I1 = Person['alve'];
// Property 'alve' does not exist on type 'Person'.
```

另一个使用任意类型进行索引的例子是使用 `number` 来获取一个数组元素的类型。我们可以把它和 `typeof` 结合起来，方便地获取一个数组字面的元素类型。

```ts
const MyArray = [
  { name: 'Alice', age: 15 },
  { name: 'Bob', age: 23 },
  { name: 'Eve', age: 38 },
];

type Person = typeof MyArray[number];
// type Person = {
// name: string;
// age: number;
// }
type Age = typeof MyArray[number]['age'];
// type Age = number
// Or
type Age2 = Person['age'];
// type Age2 = number
```

你只能在索引时使用类型，这意味着你不能使用 `const` 来做一个变量引用：

```ts
const key = 'age';
type Age = Person[key];
// 类型 'any' 不能被用作索引类型.
// 'key' 是一个值, 但是这里却被用作类型. 你是在说 'typeof key'?
```

然而，你可以使用类型别名来实现类似的重构风格。

```ts
type key = 'age';
type Age = Person[key];
```

## 条件类型

在大多数有用的程序的核心，我们必须根据输入来做决定。 JavaScript 程序也不例外，但鉴于数值可以很容易地被反省，这些决定也是基于输入的类型。条件类型有助于描述输入和输出的类型之间的关系。

```ts
interface Animal {
  live(): void;
}
interface Dog extends Animal {
  woof(): void;
}

type Example1 = Dog extends Animal ? number : string;
// type Example1 = number

type Example2 = RegExp extends Animal ? number : string;
// type Example2 = string
```

条件类型的形式看起来有点像 JavaScript 中的三元表达式（ `condition ? trueExpression : falseExpression` ）:

```ts
SomeType extends OtherType ? TrueType : FalseType;
```

当 `extends` 左边的类型可以赋值给右边的类型时，那么你将得到第一个分支中的类型（'真' 分支）；否则你将得到后一个分支中的类型（'假' 分支）。

从上面的例子来看，条件类型可能并不立即显得有用--我们可以告诉自己 `Dog` 是否 `extends` 了 `Animal` ，并选择 `number` 或 `string` ！但条件类型的威力来自于它所带来的好处。但是条件类型的力量来自于将它们与泛型一起使用。

例如，让我们来看看下面这个 `createLabel` 函数：

```ts
interface IdLabel {
  id: number /* some fields */;
}
interface NameLabel {
  name: string /* other fields */;
}

function createLabel(id: number): IdLabel;
function createLabel(name: string): NameLabel;
function createLabel(nameOrId: string | number): IdLabel | NameLabel;
function createLabel(nameOrId: string | number): IdLabel | NameLabel {
  throw 'unimplemented';
}
```

`createLabel` 的这些重载描述了一个单一的 JavaScript 函数，该函数根据其输入的类型做出选择。注意一些事情。

1.  如果一个库必须在其 API 中反复做出同样的选择，这就会变得很麻烦。
2.  我们必须创建三个重载：一个用于确定类型的情况（一个用于 `string` ，一个用于 `number` ），一个用于最一般的情况（取一个 `string | number` ）。对于 `createLabel` 所能处理的每一种新类型，重载的数量都会呈指数级增长。

作为替代，我们可以将这种逻辑编码在一个条件类型中：

```ts
type NameOrId<T extends number | string> = T extends number ? IdLabel : NameLabel;
```

然后我们可以使用该条件类型将我们的重载简化为一个没有重载的单一函数。

```ts
function createLabel<T extends number | string>(idOrName: T): NameOrId<T> {
  throw 'unimplemented';
}

let a = createLabel(' TypeScript ');
// let a: NameLabel

let b = createLabel(2.8);
// let b: IdLabel

let c = createLabel(Math.random() ? 'hello' : 42);
let c: NameLabel | IdLabel;
```

### 条件类型约束

通常，条件类型中的检查会给我们提供一些新的信息。就像用类型守卫--收窄类型的范围可以给我们一个更具体的类型一样，条件类型的真正分支将通过我们检查的类型进一步约束泛型。

例如，让我们来看看下面的例子。

```ts
type MessageOf<T> = T['message'];
// 类型 ''message'' 不能被用作索引类型 'T'.
```

在这个例子中， TypeScript 出错是因为 `T` 并不确定是否有一个叫做 `message` 的属性。我们可以对 `T` 进行约束，就不会再报错了：

```ts
type MessageOf<T extends { message: unknown }> = T['message'];

interface Email {
  message: string;
}

type EmailMessageContents = MessageOf<Email>;
// type EmailMessageContents = string
```

然而，如果我们想让 `MessageOf` 接受 `any` 类型，并在 `message` 属性不可用的情况下默认为 `never` 类型呢？我们可以通过将约束条件移出并引入一个条件类型来做到这一点：

```ts
type MessageOf<T> = T extends { message: unknown } ? T['message'] : never;

interface Email {
  message: string;
}

interface Dog {
  bark(): void;
}

type EmailMessageContents = MessageOf<Email>;
// type EmailMessageContents = string

type DogMessageContents = MessageOf<Dog>;
// type DogMessageContents = never
```

在 `true` 分支中， TypeScript 知道 `T` 会有一个 `message` 属性。

作为另一个例子，我们也可以写一个叫做 `Flatten` 的类型，将数组类型平铺到它们的元素类型上，但在其他方面则不做处理。

```ts
type Flatten<T> = T extends any[] ? T[number] : T;

type Str = Flatten<string[]>;
// type Str = string

type Num = Flatten<number>;
// type Num = number
```

当 `Flatten` 被赋予一个数组类型时，它使用一个带有数字的索引访问来获取 `string[]` 的元素类型。否则，它只是返回它被赋予的类型。

### 在条件类型中进行推断

我们刚刚发现自己使用条件类型来应用约束条件，然后提取出类型。这最终成为一种常见的操作，而条件类型使之更容易。

条件类型为我们提供了一种方法来推断我们在真实分支中使用 `infer` 关键字进行对比的类型。例如，我们可以在 `Flatten` 中推断出元素类型，而不是用索引访问类型 '手动 '提取出来。

```ts
type Flatten<Type> = Type extends Array<infer Item> ? Item : Type;
```

在这里，我们使用 `infer` 关键字来声明性地引入一个名为 `Item` 的新的泛型类型变量，而不是指定如何在真实分支中检索 `T` 的元素类型。这使我们不必考虑如何挖掘和探测我们感兴趣的类型的结构。

我们可以使用 `infer` 关键字编写一些有用的辅助类型别名。例如，对于简单的情况，我们可以从函数类型中提取出返回类型。

```ts
type GetReturnType<Type> = Type extends (...args: never[]) => infer Return ? Return : never;

type Num = GetReturnType<() => number>;
// type Num = number

type Str = GetReturnType<(x: string) => string>;
// type Str = string

type Bools = GetReturnType<(a: boolean, b: boolean) => boolean[]>;
// type Bools = boolean[]
```

当从一个具有多个调用签名的类型（如重载函数的类型）进行推断时，从最后一个签名进行推断（据推测，这是最容许的万能情况）。不可能根据参数类型的列表来执行重载解析。

```ts
declare function stringOrNum(x: string): number;
declare function stringOrNum(x: number): string;
declare function stringOrNum(x: string | number): string | number;

type T1 = ReturnType<typeof stringOrNum>;
// type T1 = string | numberTry
```

### 分布式条件类型

当条件类型作用于一个泛型类型时，当给定一个联合类型时，它们就变成了分布式的。例如，以下面的例子为例：

```ts
type ToArray<Type> = Type extends any ? Type[] : never;
```

如果我们将一个联合类型插入 `ToArray` ，那么条件类型将被应用于该联合的每个成员。

```ts
type ToArray<Type> = Type extends any ? Type[] : never;

type StrArrOrNumArr = ToArray<string | number>;
// type StrArrOrNumArr = string[] | number[]
```

这里发生的情况是， `StrArrOrNumArr` 分布于：

```ts
string | number;
```

并对联盟的每个成员类型进行映射，以达到有效的目的：

```ts
ToArray<string> | ToArray<number>;
```

输出即为：

```ts
string[] | number[]
```

如果你想避免这种行为，你可以用方括号包裹 `extends` 关键字的左右两边：

```ts
type ToArrayNonDist<Type> = [Type] extends [any] ? Type[] : never;

// 'StrOrNumArr' 不再是联合类型.
type StrOrNumArr = ToArrayNonDist<string | number>;
// type StrOrNumArr = (string | number)[]
```

## 映射类型

当你不想重复造轮子的时候，有时一个类型需要以另一个类型为基础。

映射类型建立在索引签名的语法上，索引签名用于声明没有提前声明的属性类型。

```ts
type OnlyBoolsAndHorses = {
  [key: string]: boolean | Horse;
};

const conforms: OnlyBoolsAndHorses = {
  del: true,
  rodney: false,
};
```

映射类型是一种泛型类型，它使用名为 `PropertyKey` 的联合类型（经常通过 `keyof` 创建），来遍历键名来创建一个类型：

```ts
type OptionsFlags<Type> = {
  [Property in keyof Type]: boolean;
};
```

在这个例子中， `OptionsFlags` 将从 `Type` 类型中获取所有属性，并将它们的类型改为 `boolean` 。

```ts
type FeatureFlags = {
  darkMode: () => void;
  newUserProfile: () => void;
};

type FeatureOptions = OptionsFlags<FeatureFlags>;
// type FeatureOptions = {
// darkMode: boolean;
// newUserProfile: boolean;
// }
```

### 映射修饰符

在映射过程中，有两个额外的修饰符可以应用： `readonly` 和 `?` ，它们分别影响可变性和可选性。

你可以通过用 `-` 或 `+` 作为前缀来移除或添加这些修饰符。如果你不加前缀，默认为 `+` 。

```ts
//从类型的属性中移除 '只读' 修饰符
type CreateMutable<Type> = {
  -readonly [Property in keyof Type]: Type[Property];
};

type LockedAccount = {
  readonly id: string;
  readonly name: string;
};

type UnlockedAccount = CreateMutable<LockedAccount>;
// type UnlockedAccount = {
// id: string;
// name: string;
// }
```

```ts
// 从类型的属性中移除'可选' 修饰符
type Concrete<Type> = {
  [Property in keyof Type]-?: Type[Property];
};

type MaybeUser = {
  id: string;
  name?: string;
  age?: number;
};

type User = Concrete<MaybeUser>;
// type User = {
// id: string;
// name: string;
// age: number;
// }
```

### 重映射

在 TypeScript 4.1 及以后的版本中，你可以通过映射类型中的 `as` 子句重新映射映射类型中的键：

```ts
type MappedTypeWithNewProperties<Type> = {
  [Properties in keyof Type as NewKeyType]: Type[Properties];
};
```

你可以利用模板字面量等功能，从先前的属性名称中创建新的属性名称：

```ts
type Getters<Type> = {
  [Property in keyof Type as `get${Capitalize<string & Property>}`]: () => Type[Property];
};

interface Person {
  name: string;
  age: number;
  location: string;
}

type LazyPerson = Getters<Person>;
// type LazyPerson = {
// getName: () => string;
// getAge: () => number;
// getLocation: () => string;
// }
```

你可以通过条件类型产生从不过滤掉的键。

```ts
// 移除 'kind' 属性
type RemoveKindField<Type> = {
  [Property in keyof Type as Exclude<Property, 'kind'>]: Type[Property];
};

interface Circle {
  kind: 'circle';
  radius: number;
}

type KindlessCircle = RemoveKindField<Circle>;
// type KindlessCircle = {
// radius: number;
// }
```

### 深入探索

映射类型与本类型操作部分的其他功能配合得很好，例如，这里有一个使用条件类型的映射类型，它根据一个对象的属性 `pii` 是否被设置为 `true` ，返回 `true` 或 `false` :

```ts
type ExtractPII<Type> = {
  [Property in keyof Type]: Type[Property] extends { pii: true } ? true : false;
};

type DBFields = {
  id: { format: 'incrementing' };
  name: { type: string; pii: true };
};

type foo = ExtractPII<DBFields>;
// type foo = {
// id: false;
// name: true;
// }
```

## 模版字面量

模板字面类型建立在字符串字面类型的基础上，并有能力通过联合扩展到许多字符串。

它们的语法与 JavaScript 中的模板字面字符串相同，但在类型位置上使用。当与具体的字面类型一起使用时，模板字面通过串联内容产生一个新的字符串字面类型。

```ts
type World = 'world';

type Greeting = `hello ${World}`;
// type Greeting = 'hello world'
```

当在插值位置使用联合体时，类型是每个联合体成员可能代表的所有字符串字面的集合。

```ts
type EmailLocaleIDs = 'welcome_email' | 'email_heading';
type FooterLocaleIDs = 'footer_title' | 'footer_sendoff';

type AllLocaleIDs = `${EmailLocaleIDs | FooterLocaleIDs}_id`;
// type AllLocaleIDs = 'welcome_email_id' | 'email_heading_id' | 'footer_title_id' | 'footer_sendoff_id'
```

对于模板字面中的每个插值位置，联合体都是交叉相乘的。

```ts
type AllLocaleIDs = `${EmailLocaleIDs | FooterLocaleIDs}_id`;
type Lang = 'en' | 'ja' | 'pt';

type LocaleMessageIDs = `${Lang}_${AllLocaleIDs}`;
// type LocaleMessageIDs = 'en_welcome_email_id' | 'en_email_heading_id' | 'en_footer_title_id' | 'en_footer_sendoff_id' | 'ja_welcome_email_id' | 'ja_email_heading_id' | 'ja_footer_title_id' | 'ja_footer_sendoff_id' | 'pt_welcome_email_id' | 'pt_email_heading_id' | 'pt_footer_title_id' | 'pt_footer_sendoff_id'
```

我们通常建议人们对大型字符串联合体使用提前生成，但这在较小的情况下也很有用。

### 类型中的字符串联合

模板字面量来自于根据一个类型中的现有字符串定义一个新的字符串。

例如， JavaScript 中一个常见的模式是基于一个对象当前拥有的字段来扩展它。我们将为一个函数提供一个类型定义，它增加了对 `on` 函数的支持，可以让你知道一个值何时发生了变化：

```ts
const person = makeWatchedObject({
  firstName: 'Saoirse',
  lastName: 'Ronan',
  age: 26,
});

person.on('firstNameChanged', newValue => {
  console.log(`firstName was changed to ${newValue}!`);
});
```

注意，监听事件 'firstNameChanged'，而不仅仅是 'firstName'，模板字面提供了一种方法来处理类型系统中的这种字符串操作。

```ts
type PropEventSource<Type> = {
   on(eventName: ${string & keyof Type}Changed, callback: (newValue: any) => void): void;
};

// Create a 'watched object' with an 'on' method
// so that you can watch for changes to properties.

declare function makeWatchedObject<Type>(obj: Type): Type & PropEventSource<Type>;
```

有了这个，我们可以建立一个在给定错误属性时出错的东西。

```ts
const person = makeWatchedObject({
  firstName: 'Saoirse',
  lastName: 'Ronan',
  age: 26,
});

person.on('firstNameChanged', () => {});

// It's typo-resistent
person.on('firstName', () => {});
// Argument of type ''firstName'' is not assignable to parameter of type ''firstNameChanged' | 'lastNameChanged' | 'ageChanged''.

person.on('frstNameChanged', () => {});
// Argument of type ''frstNameChanged'' is not assignable to parameter of type ''firstNameChanged' | 'lastNameChanged' | 'ageChanged''.
```

### 用模板字面量进行推理

请注意，最后一个例子没有重复使用原始值的类型。回调使用了一个 `any` 。模板字面类型可以从替换位置推断出来。

我们可以使我们的最后一个例子成为泛型的，从 `eventName` 字符串的部分推断出相关的属性：

```ts
type PropEventSource<Type> = {
   on<Key extends string & keyof Type>
       (eventName: ${Key}Changed, callback: (newValue: Type[Key]) => void ): void;
};

declare function makeWatchedObject<Type>(obj: Type): Type & PropEventSource<Type>;

const person = makeWatchedObject({
 firstName: 'Saoirse',
 lastName: 'Ronan',
 age: 26
});

person.on('firstNameChanged', newName => {
// (parameter) newName: string
   console.log(  `new name is ${newName.toUpperCase()}`  );
});

person.on('ageChanged', newAge => {
// (parameter) newAge: number
   if (newAge < 0) {
       console.warn('warning! negative age');
   }
})
```

这里我们把它变成了一个泛型方法。

当用户调用字符串 'firstNameChanged '时， TypeScript 将尝试推断出 `Key` 的正确类型。为了做到这一点，它将把 `Key` 与 'Changed '之前的内容进行匹配，并推断出字符串 'firstName'。一旦 TypeScript 发现了这一点， `on` 方法就可以在原始对象上获取 `firstName` 的类型，在这种情况下就是字符串。类似地，当调用 'ageChanged '时， TypeScript 找到了属性 `age` 的类型，即数字。

推理可以以不同的方式组合，通常是解构字符串，并以不同的方式重构它们。

### 本质上的字符串操作类型

为了帮助字符串的操作， TypeScript 包括一组可以用于字符串操作的类型。这些类型是编译器内置的性能，不能在 TypeScript 包含的 `.d.ts` 文件中找到。

#### `Uppercase<StringType>`

将字符串中的每个字符转换为大写版本：

```ts
type Greeting = 'Hello, world'
type ShoutyGreeting = Uppercase<Greeting>

type ShoutyGreeting = 'HELLO, WORLD'

type ASCIICacheKey<Str extends string> = ID-${Uppercase<Str>}
type MainID = ASCIICacheKey<'my_app'>
// type MainID = 'ID-MY_APP'
```

#### `Lowercase<StringType>`

将字符串中的每个字符转换为小写的对应字符：

```ts
type Greeting = 'Hello, world'
type QuietGreeting = Lowercase<Greeting>
// type QuietGreeting = 'hello, world'

type ASCIICacheKey<Str extends string> = id-${Lowercase<Str>}
type MainID = ASCIICacheKey<'MY_APP'>
// type MainID = 'id-my_app'
```

#### `Capitalize<StringType>`

将字符串中的第一个字符转换为大写的对应字符：

```ts
type LowercaseGreeting = 'hello, world';
type Greeting = Capitalize<LowercaseGreeting>;
// type Greeting = 'Hello, world'
```

#### `Uncapitalize<StringType>`

将字符串中的第一个字符转换为小写的对应字符：

```ts
type UppercaseGreeting = 'HELLO WORLD';
type UncomfortableGreeting = Uncapitalize<UppercaseGreeting>;
// type UncomfortableGreeting = 'hELLO WORLD'
```

# 类

TypeScript 提供了对 ES2015 中引入的 `class` 关键词的完全支持。

与其他 JavaScript 语言功能一样， TypeScript 增加了类型注释和其他语法，允许你表达类和其他类型之间的关系。

## 类成员

下面是一个最基础的类：

```ts
class Point {}
```

这个类还不是很有用，所以让我们开始添加一些成员。

### 作用域

一个字段声明在一个类上创建一个公共的可写属性：

```ts
class Point {
  x: number;
  y: number;
}

const pt = new Point();
pt.x = 0;
pt.y = 0;
```

与其他位置一样，类型注解是可选的，但如果不指定，将默认为隐式 `any` 类型。

字段也可以有初始化器；这些初始化器将在类被实例化时自动运行。

```ts
class Point {
  x = 0;
  y = 0;
}

const pt = new Point();
// Prints 0, 0
console.log(`${pt.x}, ${pt.y}`);
```

就像 `const` 、 `let` 和 `var` 一样，一个类属性的初始化器将被用来推断其类型。

```ts
const pt = new Point();
pt.x = '0';
// 不能将 'number' 分配给类型 'string'.
```

#### `--strictPropertyInitialization`

`strictPropertyInitialization` 设置控制是否需要在构造函数中初始化类字段。

```ts
class BadGreeter {
  name: string;
  // 属性 'name' 没有初始化 并且没有在构造函数里被定义.
}
```

```ts
class GoodGreeter {
  name: string;

  constructor() {
    this.name = 'hello';
  }
}
```

请注意，该字段需要在构造函数本身中初始化。 TypeScript 不会分析你从构造函数中调用的方法来检测初始化，因为派生类可能会覆盖这些方法而无法初始化成员。

如果你打算通过构造函数以外的方式来确定初始化一个字段（例如，也许一个外部库为你填充了你的类的一部分），你可以使用确定的赋值断言操作符 `!` :

```ts
class OKGreeter {
  // 没有初始化，但是没有报错
  name!: string;
}
```

#### `readonly`

字段的前缀可以是 `readonly` 修饰符。这可以防止在构造函数之外对该字段进行赋值。

```ts
class Greeter {
  readonly name: string = 'world';

  constructor(otherName?: string) {
    if (otherName !== undefined) {
      this.name = otherName;
    }
  }

  err() {
    this.name = 'not ok';
    // 不能对 'name' 进行赋值因为它是个只读属性.
  }
}
const g = new Greeter();
g.name = 'also not ok';
// 不能对 'name' 进行赋值因为它是个只读属性.
```

### Constructors

类构造函数与函数非常相似。你可以添加带有类型注释的参数、默认值和重载。

```ts
class Point {
  x: number;
  y: number;

  // 默认值
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }
}
```

```ts
class Point {
  // 重载
  constructor(x: number, y: string);
  constructor(s: string);
  constructor(xs: any, y?: any) {
    // TBD
  }
}
```

类的构造函数签名和函数签名之间只有一些区别。

- 构造函数不能有类型参数( `T` )--这属于类外层的声明，我们将在之后学到。
- 构造函数不能有返回类型注释--类的实例类型总是被返回的。

#### Super Calls

就像在 JavaScript 中一样，如果你有一个基类，在使用任何 `this.` 成员之前，你需要在构造器主体中调用 `super()` :

```ts
class Base {
  k = 4;
}

class Derived extends Base {
  constructor() {
    // Prints a wrong value in ES5; throws exception in ES6
    console.log(this.k);
    // 'super' must be called before accessing 'this' in the constructor of a derived class.
    super();
  }
}
```

在 JavaScript 中，忘记调用 `super` 是一个很容易犯的错误，但 TypeScript 会在必要时告诉你。

### Methods

一个类上的函数属性被称为方法。方法可以使用与函数和构造函数相同的所有类型注释。

```ts
class Point {
  x = 10;
  y = 10;

  scale(n: number): void {
    this.x *= n;
    this.y *= n;
  }
}
```

除了标准的类型注解， TypeScript 并没有为方法添加其他新的东西。

请注意，在一个方法体中，仍然必须通过 `this` 访问字段和其他方法。方法体中未定义变量名将总是指代方法作用域内的东西。

```ts
let x: number = 0;

class C {
  x: string = 'hello';

  m() {
    // 第一行尝试修改 'x' ，但是这个 'x' 并不是类属性
    x = 'world';
    // 不能将 'number' 类型的值赋给 'string' 类型.
  }
}
```

### Getters / Setters

类也可以拥有存取器：

```ts
class C {
  _length = 0;
  get length() {
    return this._length;
  }
  set length(value) {
    this._length = value;
  }
}
```

> 请注意，一个没有额外逻辑的字段支持的 get/set 对在 JavaScript 中很少有用。如果你不需要在 get/set 操作中添加额外的逻辑，暴露公共字段也是可以的。

TypeScript 对访问器有一些特殊的推理规则。

- 如果存在 `get` ，但没有 `set` ，则该属性自动是只读的。
- 如果没有指定 `setter` 参数的类型，它将从 `getter` 的返回类型中推断出来。
- 获取器和设置器必须有相同的成员可见性

从 TypeScript 4.3 开始，可以有不同类型的访问器用于获取和设置。

```ts
class Thing {
  _size = 0;

  get size(): number {
    return this._size;
  }

  set size(value: string | number | boolean) {
    let num = Number(value);

    // Don't allow NaN, Infinity, etc

    if (!Number.isFinite(num)) {
      this._size = 0;
      return;
    }

    this._size = num;
  }
}
```

### 索引签名

类也拥有索引签名，大致与对象的索引签名差不多：

```ts
class MyClass {
  [s: string]: boolean | ((s: string) => boolean);

  check(s: string) {
    return this[s] as boolean;
  }
}
```

因为索引签名类型需要同时捕获方法的类型，所以要有用地使用这些类型并不容易。一般来说，最好将索引数据存储在另一个地方，而不是在类实例本身。

## 类的继承

像其他具有面向对象特性的语言一样， JavaScript 中的类可以继承自基类。

### `implements` 子句

你可以使用一个 `implements` 子句来检查一个类是否满足了一个特定的接口。如果一个类不能正确地实现它，就会发出一个错误：

```ts
interface Pingable {
  ping(): void;
}

class Sonar implements Pingable {
  ping() {
    console.log('ping!');
  }
}

class Ball implements Pingable {
  // 类 'Ball' 错误实现接口 'Pingable'。
  // 类型 'Ball' 中缺少属性 'ping'，但类型 'Pingable' 中需要该属性。
  pong() {
    console.log('pong!');
  }
}
```

类也可以同时继承多个接口，比如 `class C implements A, B {}`

#### 警示

重要的是要明白， `implements` 子句只是检查类是否可以被当作接口类型来对待。它根本不会改变类的类型或其方法。一个常见的错误来源是认为 `implements` 子句会改变类的类型--它不会！。

```ts
interface Checkable {
  check(name: string): boolean;
}

class NameChecker implements Checkable {
  check(s) {
    // 参数 's' 具有隐式 'any' 类型.
    // 注意，这里将不会报错
    return s.toLowercse() === 'ok';
    // 's' 的类型为 any
  }
}
```

在这个例子中，我们也许期望 `s` 的类型会受到 `check` 的 `name: string` 参数的影响。其实不然--实现子句并没有改变类主体的检查方式或其类型的推断。

同样地，实现一个带有可选属性的接口并不能创建该属性。

```ts
interface A {
  x: number;
  y?: number;
}
class C implements A {
  x = 0;
}
const c = new C();
c.y = 10;
//  类型'C' 上不存在属性 'y'.
```

### `extends` 分句

类可以继承一个基类。派生类拥有其基类的所有属性和方法，还可以定义额外的成员。

```ts
class Animal {
  move() {
    console.log('Moving along!');
  }
}

class Dog extends Animal {
  woof(times: number) {
    for (let i = 0; i < times; i++) {
      console.log('woof!');
    }
  }
}

const d = new Dog();
// 基类方法
d.move();
// 派生类方法
d.woof(3);
```

#### 覆盖方法

派生类也可以覆盖基类的一个字段或属性。你可以使用 `super.` 语法来访问基类方法。

TypeScript 强制要求派生类始终是其基类的一个子类型。

例如，这里有一个合法的方法来覆盖一个方法。

```ts
class Base {
  greet() {
    console.log('Hello, world!');
  }
}

class Derived extends Base {
  greet(name?: string) {
    if (name === undefined) {
      super.greet();
    } else {
      console.log(`Hello, ${name.toUpperCase()}`);
    }
  }
}

const d = new Derived();
d.greet();
d.greet('reader');
```

派生类遵循其基类规则是很重要的。请记住，通过基类引用来引用派生类实例是非常常见的（而且总是合法的！）。

```ts
// Alias the derived instance through a base class reference
const b: Base = d;
// 没问题
b.greet();
```

如果 `Derived` 没有遵守 `Base` 的规则怎么办？

```ts
class Base {
  greet() {
    console.log('Hello, world!');
  }
}

class Derived extends Base {
  // 使这个参数变为必选
  greet(name: string) {
    // 'Derived' 上的属性 'greet' 不能赋值给类型 'Base' 上的同名属性.
    // 不能把类型'() => void' 赋值给类型 '(name: string) => void' .
    console.log(`Hello, ${name.toUpperCase()}`);
  }
}
```

如果我们不顾错误编译这段代码，这段代码就会崩溃。

```ts
const b: Base = new Derived();
// 崩溃的原因为 'name' 将会是 undefined
b.greet();
```

#### 初始化顺序

在某些情况下， JavaScript 类的初始化顺序可能会令人惊讶。让我们看一下这段代码：

```ts
class Base {
  name = 'base';
  constructor() {
    console.log('My name is ' + this.name);
  }
}

class Derived extends Base {
  name = 'derived';
}

// 输出 'base', 而不是 'derived'
const d = new Derived();
```

这里发生了什么？

按照 JavaScript 的定义，类初始化的顺序是：

- 基类的字段被初始化
- 基类构造函数运行
- 派生类的字段被初始化
- 派生类构造函数运行

这意味着基类构造函数在自己的构造函数中看到了自己的 `name` 值，因为派生类的字段初始化还没有运行。

#### 继承内置类型

> 注意：如果你不打算继承 `Array` 、 `Error` 、 `Map` 等内置类型，你可以跳过这一节。

在 ES2015 中，返回对象的构造函数隐含地替代了 `super(...)` 的任何调用者的 `this` 的值。生成的构造函数代码有必要捕获 `super(...)` 的任何潜在返回值并将其替换为 `this` 。

因此，子类化 `Error` 、Array` 等`可能不再像预期那样工作。这是由于 `Error` 、 `Array` 等的构造函数使用 ECMAScript 6 的 `new.target` 来调整原型链；然而，在 ECMAScript 5 中调用构造函数时，没有办法确保 `new.target` 的值。其他的下级编译器一般默认有同样的限制。

对于一个像下面这样的子类：

```ts
class MsgError extends Error {
  constructor(m: string) {
    super(m);
  }
  sayHello() {
    return 'hello ' + this.message;
  }
}
```

你可能会发现。

方法在构造这些子类所返回的对象上可能是未定义的，所以调用 `sayHello` 会导致错误。 `instanceof` 将在子类的实例和它们的实例之间被打破，所以 `(new MsgError()) instanceof MsgError` 将返回 `false` 。作为建议，你可以在任何 `super(...)` 调用后立即手动调整原型。

```ts
class MsgError extends Error {
  constructor(m: string) {
    super(m);

    // Set the prototype explicitly.
    Object.setPrototypeOf(this, MsgError.prototype);
  }

  sayHello() {
    return 'hello ' + this.message;
  }
}
```

然而， `MsgError` 的任何子类也必须手动设置原型。对于不支持 `Object.setPrototypeOf` 的运行时，你可以使用**proto**来代替。

不幸的是，这些变通方法在 **Internet Explorer 10** 和更早的版本上不起作用。我们可以手动将原型中的方法复制到实例本身（例如 `MsgError.prototype` 到 `this` ），但是原型链本身不能被修复。

## 成员可见性

你可以使用 TypeScript 来控制某些方法或属性对类外的代码是否可见。

### `public`

类成员的默认可见性是 `public` 的。一个 `public` 成员可以在任何地方被访问。

```ts
class Greeter {
  public greet() {
    console.log('hi!');
  }
}
const g = new Greeter();
g.greet();
```

因为 `public` 已经是默认的可见性修饰符，所以你永远不需要在类成员上写它，但为了风格/可读性的原因，可能会选择这样做。

### `protected`

`protected` 的成员只对它们所声明的类的子类可见。

```ts
class Greeter {
  public greet() {
    console.log('Hello, ' + this.getName());
  }
  protected getName() {
    return 'hi';
  }
}

class SpecialGreeter extends Greeter {
  public howdy() {
    // 可以在这里访问 protected 成员
    console.log('Howdy, ' + this.getName());
  }
}
const g = new SpecialGreeter();
g.greet(); // OK
g.getName();
// 属性 'getName' 受保护，只能在类 'Greeter' 及其子类中访问
```

### protected 成员的暴露

派生类需要遵循它们的基类契约，但可以选择暴露具有更多能力的基类的子类型。这包括将受保护的成员变为 `public` :

```ts
class Base {
  protected m = 10;
}
class Derived extends Base {
  // 没有修饰符，默认为 'public'
  m = 15;
}
const d = new Derived();
console.log(d.m); // OK
```

请注意， `Derived` 已经能够自由读写 `m` 了，所以这并没有意义地改变这种情况的 '安全性'。这里需要注意的是，在派生类中，如果这种暴露不是故意的，我们需要注意重复保护的修饰语。

### 跨层级的保护访问

不同的 OOP 语言对通过基类引用访问受保护成员是否合法存在分歧。

```ts
class Base {
  protected x: number = 1;
}
class Derived1 extends Base {
  protected x: number = 5;
}
class Derived2 extends Base {
  f1(other: Derived2) {
    other.x = 10;
  }
  f2(other: Base) {
    other.x = 10;
    // 属性 'x' 是受保护的并且只能在class 'Derived2' 中的实例访问. 然而这是类 'Base' 的实例.
  }
}
```

例如，Java 认为这样做是合法的。另一方面，C# 和 C++ 认为这种代码应该是非法的。

TypeScript 在这里支持 C# 和 C++，因为在 `Derived2` 中访问 `x` 应该只在 `Derived2` 的子类中是合法的，而 `Derived1` 并不是其中之一。此外，如果通过 `Derived2` 的引用访问 `x` 是非法的（当然应该是非法的！），那么通过基类的引用访问 `x` 就不应该改善这种状况。

### `private`

`private` 和 `protected` 一样，但不允许从子类中访问该成员。

```ts
class Base {
  private x = 0;
}
const b = new Base();
// 不能在类的外部访问
console.log(b.x);
// 属性 'x' 为私有属性，只能在类 'Base' 中访问
```

```ts
class Derived extends Base {
  showX() {
    // 在子类中不能访问
    console.log(this.x);
    // 属性 'x' 为私有属性，只能在类 'Base' 中访问
  }
}
```

因为私有成员对派生类是不可见的，所以派生类不能增加其可见性。

```ts
class Base {
  private x = 0;
}
class Derived extends Base {
  // 类 'Derived' 不正确的继承基类 'Base'.
  // 属性 'x' 在类 'Base' 中是私有的但是在类中 'Derived' 却并不是.
  x = 1;
}
```

### 跨实例的私有访问

不同的 OOP 语言对于同一个类的不同实例是否可以访问对方的私有成员有不同的意见。虽然像 Java、C#、C++、Swift 和 PHP 等语言允许这样做，但 Ruby 不允许。

TypeScript 确实允许跨实例的私有访问：

```ts
class A {
  private x = 10;

  public sameAs(other: A) {
    // 没问题
    return other.x === this.x;
  }
}
```

### 警告

像 TypeScript 类型系统的其他方面一样， `private` 和 `protected` 只在类型检查中被强制执行。这意味着 JavaScript 的运行时结构，如 `in` 或简单的属性查询，仍然可以访问一个 `private` 或 `protected` 的成员：

```ts
class MySafe {
  private secretKey = 12345;
}
```

```ts
// 在  JavaScript  文件中...
const s = new MySafe();
// 将会输出 12345
console.log(s.secretKey);
```

如果你需要保护你的类中的值免受恶意行为的影响，你应该使用提供硬运行时隐私的机制，例如闭包、弱映射或私有字段。

## 静态类型成员

类可以有静态成员。这些成员并不与类的特定实例相关联。它们可以通过类的构造函数对象本身来访问。

```ts
class MyClass {
  static x = 0;
  static printX() {
    console.log(MyClass.x);
  }
}
console.log(MyClass.x);
MyClass.printX();
```

静态成员也可以使用相同的 `public` 、 `protected` 和 `private` 可见性修饰符。

```ts
class MyClass {
  private static x = 0;
}
console.log(MyClass.x);
// 属性 'x' 是私有的并且只能在类 'MyClass' 内部访问.
```

静态成员也可以继承：

```ts
class Base {
  static getGreeting() {
    return 'Hello world';
  }
}
class Derived extends Base {
  myGreeting = Derived.getGreeting();
}
```

### 特殊静态名称

一般来说，从函数原型覆盖属性是不安全的/不可能的。因为类本身就是可以用 `new` 调用的函数，所以某些静态名称不能使用。像 `name` 、 `length` 和 `call` 这样的函数属性，定义为静态成员是无效的。

```ts
class S {
  static name = 'S!';
  // Static property 'name' conflicts with built-in property 'Function.name' of constructor function 'S'.
}
```

### 为什么不推荐静态类？

TypeScript （和 JavaScript ）没有像 C# 和 Java 那样有一个叫做静态类的结构。

这些结构体的存在只是因为这些语言强制所有的数据和函数都在一个类里面；因为这个限制在 TypeScript 中不存在，所以不需要它们。一个只有一个实例的类，在 JavaScript / TypeScript 中通常只是表示为一个普通的对象。

例如，我们不需要 TypeScript 中的 '静态类 '语法，因为一个普通的对象（甚至是顶级函数）也可以完成这个工作。

```ts
// 没有必须必要的静态类型
class MyStaticClass {
  static doSomething() {}
}

// 推荐 (方案 1)
function doSomething() {}

// 推荐 (方案 2)
const MyHelperObject = {
  dosomething() {},
};
```

## 泛型类

类，和接口一样，可以是泛型的。当一个泛型类用 `new` 实例化时，其类型参数的推断方式与函数调用的方式相同。

```ts
class Box<Type> {
  contents: Type;
  constructor(value: Type) {
    this.contents = value;
  }
}

const b = new Box('hello!');
// const b: Box<string>
```

类可以像接口一样使用通用约束和默认值。

### 静态成员中的类型参数

这段代码是不合法的，原因可能并不明显。

```ts
class Box<Type> {
  static defaultValue: Type;
  // 静态成员不能引用类的类型参数.
}
```

请记住，类型总是被完全擦除的！在运行时，只有一个 `Box.defaultValue` 属性槽。这意味着设置 `Box<string>.defaultValue` （如果有可能的话）也会改变 `Box<number>.defaultValue` --这可不是什么好事。一个泛型类的静态成员永远不能引用该类的类型参数。

## 类在运行时的 this

重要的是要记住， TypeScript 并没有改变 JavaScript 的运行时行为，而 JavaScript 因为有一些奇特的运行时行为而有点出名。

JavaScript 对这一点的处理确实是不寻常的。

```ts
class MyClass {
  name = 'MyClass';
  getName() {
    return this.name;
  }
}
const c = new MyClass();
const obj = {
  name: 'obj',
  getName: c.getName,
};

// Prints 'obj', not 'MyClass'
console.log(obj.getName());
```

长话短说，默认情况下，函数内 this 的值取决于函数的调用方式。在这个例子中，因为函数是通过 `obj` 引用调用的，所以它的 `this` 值是 `obj` 而不是类实例。

这很少是你希望发生的事情! TypeScript 提供了一些方法来减轻或防止这种错误。

### 箭头函数

如果你有一个经常会被调用的函数，失去了它的这个上下文，那么使用一个箭头函数属性而不是方法定义是有意义的。

```ts
class MyClass {
  name = 'MyClass';
  getName = () => {
    return this.name;
  };
}
const c = new MyClass();
const g = c.getName;
// Prints 'MyClass' instead of crashing
console.log(g());
```

这有一些权衡。

- 这个值保证在运行时是正确的，即使是没有经过 TypeScript 检查的代码也是如此。
- 这将使用更多的内存，因为每个类实例将有它自己的副本，每个函数都是这样定义的
- 你不能在派生类中使用 `super.getName` ，因为在原型链中没有条目可以从基类方法中获取

### this 参数

在方法或函数定义中，一个名为 `this` 的初始参数在 TypeScript 中具有特殊的意义。这些参数在编译过程中会被抹去。

```ts
//  TypeScript  输入 'this' 参数
function fn(this: SomeType, x: number) {
  /* ... */
}
```

```ts
//  JavaScript  输出
function fn(x) {
  /* ... */
}
```

TypeScript 检查调用带有 `this` 参数的函数是否在正确的上下文中进行。我们可以不使用箭头函数，而是在方法定义中添加一个 `this` 参数，以静态地确保方法被正确调用。

```ts
class MyClass {
  name = 'MyClass';
  getName(this: MyClass) {
    return this.name;
  }
}
const c = new MyClass();
// OK
c.getName();

// 错误，将会崩溃
const g = c.getName;
console.log(g());
// 类型为'void'的 'this' 上下文不能分配给类型为'MyClass'的方法的 'this'
```

这种方法采取了与箭头函数方法相反的取舍。

- JavaScript 调用者仍然可能在不知不觉中错误地使用类方法
- 每个类定义只有一个函数被分配，而不是每个类实例一个函数
- 基层方法定义仍然可以通过 super 调用。

## this 类型

在类中，一个叫做 `this` 的特殊类型动态地指向当前类的类型。让我们来看看这有什么用。

```ts
class Box {
 contents: string = '';
 set(value: string) {

(method) Box.set(value: string): this
   this.contents = value;
   return this;
 }
}
```

在这里， TypeScript 推断出 `set` 的返回类型是 `this` ，而不是 `Box` 。现在让我们做一个 `Box` 的子类。

```ts
class ClearableBox extends Box {
  clear() {
    this.contents = '';
  }
}

const a = new ClearableBox();
const b = a.set('hello');
// const b: ClearableBox
```

你也可以在参数类型注解中使用 `this` 。

```ts
class Box {
  content: string = '';
  sameAs(other: this) {
    return other.content === this.content;
  }
}
```

这与写其他不同。框--如果你有一个派生类，它的 `sameAs` 方法现在只接受该同一派生类的其他实例。

```ts
class Box {
  content: string = '';
  sameAs(other: this) {
    return other.content === this.content;
  }
}

class DerivedBox extends Box {
  otherContent: string = '?';
}

const base = new Box();
const derived = new DerivedBox();
derived.sameAs(base);
// Argument of type 'Box' is not assignable to parameter of type 'DerivedBox'.
// Property 'otherContent' is missing in type 'Box' but required in type 'DerivedBox'.
```

### `this` -基本类型守卫

你可以在类和接口的方法的返回位置使用这个是类型。当与类型缩小混合时（例如 if 语句），目标对象的类型将被缩小到指定的 `Type` 。

```ts
class FileSystemObject {
  isFile(): this is FileRep {
    return this instanceof FileRep;
  }
  isDirectory(): this is Directory {
    return this instanceof Directory;
  }
  isNetworked(): this is Networked & this {
    return this.networked;
  }
  constructor(public path: string, private networked: boolean) {}
}

class FileRep extends FileSystemObject {
  constructor(path: string, public content: string) {
    super(path, false);
  }
}

class Directory extends FileSystemObject {
  children: FileSystemObject[];
}

interface Networked {
  host: string;
}

const fso: FileSystemObject = new FileRep('foo/bar.txt', 'foo');

if (fso.isFile()) {
  fso.content;

  const fso: FileRep;
} else if (fso.isDirectory()) {
  fso.children;

  const fso: Directory;
} else if (fso.isNetworked()) {
  fso.host;

  const fso: Networked & FileSystemObject;
}
```

基于 `this` 的类型保护的一个常见用例是允许对一个特定字段进行懒惰验证。例如，这种情况下，当 `hasValue` 被验证为真时，就会从框内持有的值中删除一个未定义值。

```ts
class Box<T> {
  value?: T;

  hasValue(): this is { value: T } {
    return this.value !== undefined;
  }
}

const box = new Box();
box.value = 'Gameboy';

box.value;
// (property) Box<unknown>.value?: unknown

if (box.hasValue()) {
  box.value;
  // (property) value: unknown
}
```

## 参数属性

TypeScript 提供了特殊的语法，可以将构造函数参数变成具有相同名称和值的类属性。这些被称为参数属性，通过在构造函数参数前加上可见性修饰符 `public` 、 `private` 、 `protected` 或 `readonly` 中的一个来创建。由此产生的字段会得到这些修饰符。

```ts
class Params {
  constructor(public readonly x: number, protected y: number, private z: number) {
    // No body necessary
  }
}
const a = new Params(1, 2, 3);
console.log(a.x);
// (property) Params.x: number
console.log(a.z);
// Property 'z' is private and only accessible within class 'Params'.
```

## 类表达式

类表达式与类声明非常相似。唯一真正的区别是，类表达式不需要一个名字，尽管我们可以通过它们最终绑定的任何标识符来引用它们。

```ts
const someClass = class<Type> {
  content: Type;
  constructor(value: Type) {
    this.content = value;
  }
};

const m = new someClass('Hello, world');
// const m: someClass<string>
```

## 抽象类

TypeScript 中的类、方法和字段可以是抽象的。

一个抽象的方法或抽象的字段是一个没有提供实现的方法或字段。这些成员必须存在于一个抽象类中，不能直接实例化。

抽象类的作用是作为子类的基类，实现所有的抽象成员。当一个类没有任何抽象成员时，我们就说它是具体的。

让我们看一个例子

```ts
abstract class Base {
  abstract getName(): string;

  printName() {
    console.log('Hello, ' + this.getName());
  }
}

const b = new Base();
// Cannot create an instance of an abstract class.
```

我们不能用 `new` 来实例化 `Base` ，因为它是抽象的。相反，我们需要创建一个派生类并实现抽象成员。

```ts
class Derived extends Base {
  getName() {
    return 'world';
  }
}

const d = new Derived();
d.printName();
```

注意，如果我们忘记实现基类的抽象成员，我们会得到一个错误。

```ts
class Derived extends Base {
  // Non-abstract class 'Derived' does not implement inherited abstract member 'getName' from class 'Base'.
  // forgot to do anything
}
```

### 抽象构造签名

有时你想接受一些类的构造函数，产生一个从某些抽象类派生出来的类的实例。

例如，你可能想写这样的代码。

```ts
function greet(ctor: typeof Base) {
  const instance = new ctor();
  // Cannot create an instance of an abstract class.
  instance.printName();
}
```

TypeScript 正确地告诉你，你正试图实例化一个抽象类。毕竟，鉴于 `greet` 的定义，写这段代码是完全合法的，它最终会构造一个抽象类。

```ts
// Bad!
greet(Base);
```

相反，你想写一个函数，接受具有结构化签名的东西。

```ts
function greet(ctor: new () => Base) {
  const instance = new ctor();
  instance.printName();
}
greet(Derived);
greet(Base);
// Argument of type 'typeof Base' is not assignable to parameter of type 'new () => Base'.
// Cannot assign an abstract constructor type to a non-abstract constructor type.
```

现在 TypeScript 正确地告诉你哪些类的构造函数可以被调用--Derived 可以，因为它是具体的，但 Base 不能。

## 类之间的关系

在大多数情况下， TypeScript 中的类在结构上是比较的，与其他类型相同。

例如，这两个类可以相互替代使用，因为它们是相同的。

```ts
class Point1 {
  x = 0;
  y = 0;
}

class Point2 {
  x = 0;
  y = 0;
}

// OK
const p: Point1 = new Point2();
```

同样地，即使没有明确的继承，类之间的子类型关系也是存在的。

```ts
class Person {
  name: string;
  age: number;
}

class Employee {
  name: string;
  age: number;
  salary: number;
}

// OK
const p: Person = new Employee();
```

这听起来很简单，但有几种情况似乎比其他情况更奇怪。

空的类没有成员。在一个结构化类型系统中，一个没有成员的类型通常是其他任何东西的超类型。所以如果你写了一个空类（不要！），任何东西都可以用来代替它。

```ts
class Empty {}

function fn(x: Empty) {
  // can't do anything with 'x', so I won't
}

// All OK!
fn(window);
fn({});
fn(fn);
```

# 模块

JavaScript 有很长的历史，有不同的方式来处理模块化的代码。 TypeScript 从 2012 年开始出现，已经实现了对许多这些格式的支持，但随着时间的推移，社区和 JavaScript 规范已经趋向于一种名为 ES 模块（或 ES6 模块）的格式。你可能知道它是 `import / export` 语法。

ES Modules 在 2015 年被加入到 JavaScript 规范中，到 2020 年，在大多数网络浏览器和 JavaScript 运行时中都有广泛的支持。

为了突出重点，本手册将涵盖 ES Modules 及其流行的前驱 `CommonJS module.exports = ` 语法，你可以在模块下的参考资料部分找到关于其他模块模式的信息。

## 如何定义 JavaScript 模块

在 TypeScript 中，就像在 ECMAScript 2015 中一样，任何包含顶级导入或导出的文件都被认为是一个模块。

相反，一个没有任何顶级导入或导出声明的文件被视为一个脚本，其内容可在全局范围内使用（因此也可用于模块）。

模块在自己的范围内执行，而不是在全局范围内。这意味着在模块中声明的变量、函数、类等在模块外是不可见的，除非它们被明确地用某种导出形式导出。相反，要使用从不同模块导出的变量、函数、类、接口等，必须使用导入的形式将其导入。

## 无模块

在我们开始之前，重要的是要了解 TypeScript 认为什么是模块。 JavaScript 规范声明，任何没有导出或顶层等待的 JavaScript 文件都应该被认为是一个脚本而不是一个模块。

在一个脚本文件中，变量和类型被声明为在共享的全局范围内，并且假定你会使用 `--outFile` 编译器选项将多个输入文件连接到一个输出文件，或者在你的 HTML 中使用多个 `<script>` 标签来加载这些文件（以正确的顺序！）。

如果你有一个目前没有任何导入或导出的文件，但你希望被当作一个模块来处理，请添加这一行。

```ts
export {};
```

这将改变该文件，使其成为一个什么都不输出的模块。无论你的模块目标是什么，这个语法都有效。

## TypeScript 中的模块

在 TypeScript 中编写基于模块的代码时，有三个主要方面需要考虑。

- 语法：我想用什么语法来导入和导出东西？
- 模块解决：模块名称（或路径）和磁盘上的文件之间有什么关系？
- 模块输出目标：我发射的 JavaScript 模块应该是什么样子的？

### ES 模块语法

一个文件可以通过出口默认值声明一个主要的出口。

```ts
// @filename: hello.ts
export default function helloWorld() {
  console.log('Hello, world!');
}
```

然后使用 `import` 导入就像这样：

```ts
import hello from './hello.js';
hello();
```

除了默认的导出，你还可以通过省略默认的导出，有一个以上的变量和函数的导出。

```ts
// @filename: maths.ts
export var pi = 3.14;
export let squareTwo = 1.41;
export const phi = 1.61;

export class RandomNumberGenerator {}

export function absolute(num: number) {
  if (num < 0) return num * -1;
  return num;
}
```

这些可以通过导入语法在另一个文件中使用。

```ts
import { pi, phi, absolute } from './maths.js';

console.log(pi);
const absPhi = absolute(phi);
// const absPhi: number
```

### 额外的导入语法

可以使用 import {old as new} 这样的格式来重命名一个导入。

```ts
import { pi as π } from './maths.js';

console.log(π);
// (alias) var π: number
// import π
```

你可以将上述语法混合并匹配到一个单一的导入中。

```ts
// @filename: maths.ts
export const pi = 3.14;
export default class RandomNumberGenerator {}

// @filename: app.ts
import RNGen, { pi as π } from './maths.js';
```

你可以把所有导出的对象，用`*`作为名称，把它们放到一个命名空间。

```ts
// @filename: app.ts
import * as math from './maths.js';

console.log(math.pi);
const positivePhi = math.absolute(math.phi);
```

你可以通过导入'./file '导入一个文件，而不把任何变量纳入你的当前模块。

```ts
// @filename: app.ts
import './maths.js';

console.log('3.14');
```

在这种情况下，导入没有任何作用。然而，maths.ts 中的所有代码都被评估了，这可能引发影响其他对象的副作用。

#### TypeScript Specific ES Module Syntax

类型可以使用与 JavaScript 值相同的语法进行导出和导入。

```ts
// @filename: animal.ts
export type Cat = { breed: string; yearOfBirth: number };

export interface Dog {
  breeds: string[];
  yearOfBirth: number;
}

// @filename: app.ts
import { Cat, Dog } from './animal.js';
type Animals = Cat | Dog;
```

TypeScript 用 import type 扩展了导入语法，它是一个只能导入类型的导入。

```ts
// @filename: animal.ts
export type Cat = { breed: string; yearOfBirth: number };
// 'createCatName' cannot be used as a value because it was imported using 'import type'.
export type Dog = { breeds: string[]; yearOfBirth: number };
export const createCatName = () => 'fluffy';

// @filename: valid.ts
import type { Cat, Dog } from './animal.js';
export type Animals = Cat | Dog;

// @filename: app.ts
import type { createCatName } from './animal.js';
const name = createCatName();
```

这种语法允许像 Babel、swc 或 esbuild 这样的非 TypeScript 转译器知道哪些导入可以被安全删除。

#### ES Module Syntax with CommonJS Behavior

TypeScript 有 ES Module 语法，它直接与 CommonJS 和 AMD 的 require 相关联。使用 ES Module 的导入在大多数情况下与这些环境中的 require 相同，但这种语法确保你在 TypeScript 文件中与 CommonJS 的输出有 1 对 1 的匹配。

```ts
import fs = require('fs');
const code = fs.readFileSync('hello.ts', 'utf8');
```

## CommonJS Syntax

CommonJS 是 npm 上大多数模块的交付格式。即使你使用上面的 ES 模块语法进行编写，对 CommonJS 语法的工作方式有一个简单的了解也会帮助你更容易地进行调试。

#### Exporting

标识符是通过在一个全局调用的模块上设置出口属性来导出的。

```ts
function absolute(num: number) {
  if (num < 0) return num * -1;
  return num;
}

module.exports = {
  pi: 3.14,
  squareTwo: 1.41,
  phi: 1.61,
  absolute,
};
```

然后这些文件可以通过 require 语句导入：

```ts
const maths = require('maths');
maths.pi;
// any
```

或者你可以使用 JavaScript 中的解构功能来简化一下。

```ts
const { squareTwo } = require('maths');
squareTwo;
// const squareTwo: any
```

### CommonJS 和 ES 模块的互操作

在 CommonJS 和 ES Module 之间有一个错误的功能匹配，因为 ES Modules 只支持将默认导出作为一个对象，而不支持作为一个函数。 TypeScript 有一个编译器标志，可以通过 esModuleInterop 减少两套不同约束之间的摩擦。

## TypeScript 的模块解析选项

模块解析是指从 import 或 require 语句中获取一个字符串，并确定该字符串所指的文件的过程。

TypeScript 包括两种解析策略。经典和节点。当编译器标志模块不是 commonjs 时，经典策略是默认的，是为了向后兼容。Node 策略复制了 Node.js 在 CommonJS 模式下的工作方式，对.ts 和.d.ts 有额外的检查。

在 TypeScript 中，有许多 TSConfig 标志影响模块策略：moduleResolution、baseUrl、paths、rootDirs。

关于这些策略如何工作的全部细节，你可以参考《模块解析》。

## TypeScript 的模块输出选项

有两个选项会影响输出的 JavaScript 。

`target` ，它决定了哪些 JS 功能被降级（转换为在旧的 JavaScript 运行时运行），哪些被保留。模块，它决定了哪些代码用于模块之间的相互作用。你使用的目标是由你期望运行 TypeScript 代码的 JavaScript 运行时中的可用功能决定的。这可能是：你支持的最古老的网络浏览器，你期望运行的最低版本的 Node.js，或者可能来自于你的运行时的独特约束--比如 Electron。

所有模块之间的通信都是通过模块加载器进行的，编译器的标志模块决定了使用哪一个。在运行时，模块加载器负责在执行一个模块之前定位和执行该模块的所有依赖项。

例如，这里是一个使用 ES 模块语法的 TypeScript 文件，展示了模块的一些不同选项。

```ts
import { valueOfPi } from './constants.js';

export const twoPi = valueOfPi * 2;
```

#### `ES2020`

```ts
import { valueOfPi } from './constants.js';
export const twoPi = valueOfPi * 2;
Try;
```

#### `CommonJS`

```ts
'use strict';
Object.defineProperty(exports, '__esModule', { value: true });
exports.twoPi = void 0;
const constants_js_1 = require('./constants.js');
exports.twoPi = constants_js_1.valueOfPi * 2;
```

#### `UMD`

```ts
(function (factory) {
   if (typeof module === 'object' && typeof module.exports === 'object') {
       var v = factory(require, exports);
       if (v !== undefined) module.exports = v;
   }
   else if (typeof define === 'function' && define.amd) {
       define(['require', 'exports', './constants.js'], factory);
   }
})(function (require, exports) {
   'use strict';
   Object.defineProperty(exports, '__esModule', { value: true });
   exports.twoPi = void 0;
   const constants_js_1 = require('./constants.js');
   expo
```

> 注意 ES2020 和 index.ts 原始内容一样

## TypeScript 命名空间

JavaScript 有很长的历史，有不同的方式来处理模块化的代码。TypeScript 从 2012 年开始出现，已经实现了对许多这些格式的支持，但随着时间的推移，社区和 JavaScript 规范已经汇聚到一种叫做 ES 模块（或 ES6 模块）的格式上。你可能知道它是 `import/export` 语法。

ES Modules 在 2015 年被加入到 JavaScript 规范中，到 2020 年，在大多数网络浏览器和 JavaScript 运行时中都有广泛的支持。

为了突出重点，本手册将涵盖 ES Modules 及其流行的前驱 CommonJS module.exports = 语法，你可以在模块下的参考资料部分找到关于其他模块模式的信息。

### JavaScript 中的模块是怎么定义的

在 TypeScript 中，就像在 ES6 中一样，任何包含顶级 `import` 或 `export` 的文件都被认为是一个模块。

相反，一个没有任何顶级 `import` 或 `export` 声明的文件被视为一个脚本，其内容可在全局范围内使用（因此也可用于模块）。

模块在自己的作用域内执行，而不是在全局作用域内。这意味着在模块中声明的变量、函数、类等在模块外是不可见的，除非它们被明确地用某种 `export` 形式导出。相反，要使用从不同模块导出的变量、函数、类、接口等，必须使用 `import` 的形式将其导入。

### 非模块

在我们开始之前，重要的是要了解 TypeScript 认为什么是模块。JavaScript 规范声明，任何没有导出或顶层等待的 JavaScript 文件都应该被认为是一个脚本而不是一个模块。

在一个脚本文件中，变量和类型被声明为在共享的全局范围内，并且假定你会使用 outFile 编译器选项将多个输入文件加入一个输出文件，或者在你的 HTML 中使用多个<script>标签来加载这些文件（顺序正确！）。

如果你有一个目前没有任何 `import` 或 `export` 的文件，但你希望被当作一个模块来处理，请添加这一行:

```ts
export {};
```

这将改变该文件，使其成为一个什么都不输出的模块。无论你的模块目标是什么，这个语法都有效。

### Typescript 中的模块

在 TypeScript 中编写基于模块的代码时，有三个主要方面需要考虑:

- 语法：我想用什么语法 `import` `export`变量？
- 模块解析：模块路径和磁盘上的文件之间是什么关系？
- 模块输出目标：我生成的 JavaScript 模块应该是什么样子的？

#### ES Module 语法

可以使 `export default` 出默认模块:

```ts
export default function helloWorld() {
  console.log('Hello, world!');
}
```

使用 `import` 导入模块:

```ts
import helloWorld from './hello.js';
helloWorld();
```

还可以导出多个变量:

```ts
export var pi = 3.14;
export let squareTwo = 1.41;
export const phi = 1.61;

export class RandomNumberGenerator {}

export function absolute(num: number) {
  if (num < 0) return num * -1;
  return num;
}
```

可以在其他文件使用 `import` 像这样导入:

```ts
import { pi, phi, absolute } from './maths.js';
```

#### 其他的导入语法

可以使用 `import {old as new}` 这样的格式来重命名一个导入:

```ts
import { pi as π } from './maths.js';
```

你可以使用 `* as name` 语法一次性导入所有成员:

```ts
import * as math from './maths.js';
```

你可以使用`import "./file "`导入一个文件，而不把任何变量纳入你的当前模块。

```ts
import './maths.js';
```

在这种情况下，`import` 没有导入任何变量。但是 `maths.ts` 中的所有代码都执行了，这可能引发影响其他对象的副作用。

#### Typescript 中的 ES Module 语法

类型可以使用与 JavaScript 值相同的语法进 `import` `export`。

TypeScript 有它自己的模块格式，叫做命名空间，这比 ES 模块标准要早。这种语法对于创建复杂的定义文件有很多有用的功能，并且在 `DefinitelyTyped` 中仍然看到积极的使用。虽然没有被废弃，但命名空间中的大部分功能都存在于 ES Modules 中，我们建议你使用它来与 JavaScript 的方向保持一致。你可以在 `namespaces` 参考页中了解更多关于命名空间的信息。
